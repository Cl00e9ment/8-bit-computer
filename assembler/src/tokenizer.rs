use std::iter::{Peekable, Enumerate};
use std::str::Chars;

use codemap::{Span, File};

use crate::errors::AssemblerError;

#[derive(Clone)]
pub struct Token {
	pub kind: TokenKind,
	pub span: Span,
}

#[derive(PartialEq, Clone, Copy)]
pub enum TokenKind {
	Dot,
	Colon,
	Plus,
	LBracket,
	RBracket,
	Identifier,
	LitteralBin,
	LitteralDec,
	LitteralHex,
	EndOfLine,
}

pub struct Tokenizer<'a> {
	chars: Peekable<Enumerate<Chars<'a>>>,
	file_span: Span,
	last_tok_was_eol: bool,
}

impl<'a> Tokenizer<'a> {
	pub fn from(source_file: &'a File) -> Tokenizer<'a> {
		Tokenizer {
			chars: source_file.source().chars().enumerate().peekable(),
			file_span: source_file.span,
			last_tok_was_eol: true,
		}
	}

	pub fn into_tokens(mut self) -> Result<Vec<Token>, AssemblerError> {
		let mut tokens = Vec::new();
		while let Some(tok) = self.next_tok()? {
			tokens.push(tok);
		}
		tokens.shrink_to_fit();
		Ok(tokens)
	}

	fn next_tok(&mut self) -> Result<Option<Token>, AssemblerError> {
		// consume characters until a token is reached, and merge succeeding line breaks into a single one
		self.consume_non_tokens(self.last_tok_was_eol);

		match self.chars.next() {
			Some((i,'.')) => self.gen_token(TokenKind::Dot, i, 1),
			Some((i, ':')) => self.gen_token(TokenKind::Colon, i, 1),
			Some((i, '+')) => self.gen_token(TokenKind::Plus, i, 1),
			Some((i, '[')) => self.gen_token(TokenKind::LBracket, i, 1),
			Some((i, ']')) => self.gen_token(TokenKind::RBracket, i, 1),
			Some((i, '\n')) => self.gen_token(TokenKind::EndOfLine, i, 1),
			Some((i, 'a'..='z' | 'A'..='Z' | '_'))  => {
				let mut len = 1;
				while self.chars.next_if(|char| matches!(char.1, 'a'..='z' | 'A'..='Z' | '_' | '0'..='9')).is_some() {
					len += 1;
				}
				self.gen_token(TokenKind::Identifier, i, len)
			},
			Some((i, '0')) if self.chars.next_if(|char| char.1 == 'b').is_some() => {
				let mut len = 2;
				while self.chars.next_if(|char| char.1 == '0' || char.1 == '1').is_some() {
					len += 1;
				}
				if len == 2 {
					self.gen_error("expected digits after binary prefix", i, len)
				} else {
					self.gen_token(TokenKind::LitteralBin, i, len)
				}
			},
			Some((i, '0')) if self.chars.next_if(|char| char.1 == 'x').is_some() => {
				let mut len = 2;
				while self.chars.next_if(|char| matches!(char.1, '0'..='9' | 'a'..='f' | 'A'..='F')).is_some() {
					len += 1;
				}
				if len == 2 {
					self.gen_error("expected digits after hex prefix", i, len)
				} else {
					self.gen_token(TokenKind::LitteralHex, i, len)
				}
			},
			Some((i, '0'..='9')) => {
				let mut len = 1;
				while self.chars.next_if(|char| char.1 >= '0' && char.1 <= '9').is_some() {
					len += 1;
				}
				self.gen_token(TokenKind::LitteralDec, i, len)
			},
			Some((i, _)) => self.gen_error("unexpected char", i, 1),
			// force last token to be EOL
			None if !self.last_tok_was_eol => self.gen_token(TokenKind::EndOfLine, self.file_span.len() as usize, 0),
			// no more token to parse
			None => Ok(None),
		}
    }

	/// Consumes characters until a token is reached.
	/// Useful for ignoring blanks and comments.
	fn consume_non_tokens(&mut self, consume_line_breaks: bool) {
		loop {
			match self.chars.peek() {
				Some((_, ';')) => {
					self.chars.next();
					while self.chars.peek().filter(|char| char.1 != '\n').is_some() {
						self.chars.next();
					}
				},
				Some((_, '\r' | '\t' | ' ')) => {
					self.chars.next();
				},
				Some((_, '\n')) if consume_line_breaks => {
					self.chars.next();
				},
				_ => break,
			}
		}
	}

	fn gen_token(&mut self, kind: TokenKind, span_start: usize, span_len: usize) -> Result<Option<Token>, AssemblerError> {
		self.last_tok_was_eol = kind == TokenKind::EndOfLine;
		Ok(Some(Token { kind, span: self.file_span.subspan(span_start as u64, (span_start + span_len) as u64) }))
	}

	fn gen_error(&mut self, message: &'static str, span_start: usize, span_len: usize) -> Result<Option<Token>, AssemblerError> {
		Err(AssemblerError { message, span: self.file_span.subspan(span_start as u64, (span_start + span_len) as u64) })
	}
}
