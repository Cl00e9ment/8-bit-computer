mod errors;
mod isa;
mod parser;
mod tokenizer;

use std::{fs, io::Write};
use std::fs::File;

use clap::Parser as ClapParser;

use codemap::CodeMap;
use errors::{abort_with_msg, abort_with_asm_err};
use parser::Parser;
use tokenizer::Tokenizer;

#[derive(ClapParser)]
#[command(version, about)]
struct Args {
	/// Source file
	input: String,

	/// Output file
	#[arg(short, default_value="a.out")]
	output: String,
}

fn main() {
	let args = Args::parse();

	let source_file_content = fs::read_to_string(&args.input)
		.map_err(|e| format!("failed to load {}: {}", args.input, e))
		.unwrap_or_else(|e| abort_with_msg(e));

	let mut output_file = File::create(&args.output)
		.map_err(|e| format!("failed to create {}: {}", args.output, e))
		.unwrap_or_else(|e| abort_with_msg(e));

	let mut codemap = CodeMap::new();
	let source_file = codemap.add_file(args.input, source_file_content);

	let machine_code = Tokenizer::from(&source_file).into_tokens()
		.and_then(|tokens| Parser::from(&source_file, &tokens).into_machine_code())
		.unwrap_or_else(|err| abort_with_asm_err(err, &codemap));

	output_file.write_all(&machine_code)
		.map_err(|e| format!("failed to write to {}: {}", args.output, e))
		.unwrap_or_else(|e| abort_with_msg(e));
}
