use std::collections::HashMap;
use std::num::IntErrorKind;

use codemap::{File, Span};

use crate::errors::AssemblerError;
use crate::isa::{
	OperandType, INSTRUCTIONS, OPCODE_JMP,
	OPCODE_POP, OPCODE_PUSH, OPERAND_CODE_AD, OPERAND_CODE_SP,
	REGISTERS, REGCODE_LIP, REGCODE_HIP,
};
use crate::tokenizer::{Token, TokenKind};

const INTRINSIC_VAR_VOID      : &str = "void";
const INTRINSIC_VAR_RET_ADDR_L: &str = "ret_addr_l";
const INTRINSIC_VAR_RET_ADDR_H: &str = "ret_addr_h";

struct FuncCtx {
	stack_header_size: usize,
	func_start_span: Span,
}

pub struct Operand {
	pub kind: OperandType,
	pub encoding: u16,
}

pub struct Parser<'a> {
	source_file: &'a File,
	tokens: &'a Vec<Token>,
	cursor: usize,

	machine_code: Vec<u16>,

	jmp_src: HashMap<usize, Span>,
	jmp_dst: HashMap<&'a str, usize>,

	stack_ctx: Vec<&'a str>,
	saved_stack_ctxs: HashMap<&'a str, Vec<&'a str>>,
	current_func_ctx: Option<FuncCtx>,
}

impl<'a> Parser<'a> {
	pub fn from(source_file: &'a File, tokens: &'a Vec<Token>) -> Parser<'a> {
		Parser {
			source_file,
			tokens,
			cursor: 0,
			machine_code: Vec::new(),
			jmp_src: HashMap::new(),
			jmp_dst: HashMap::new(),
			stack_ctx: Vec::new(),
			saved_stack_ctxs: HashMap::new(),
			current_func_ctx: None,
		}
	}

	pub fn into_machine_code(mut self) -> Result<Vec<u8>, AssemblerError> {
		while self.cursor < self.tokens.len() {
			if !(self.try_parse_pragma()? || self.try_parse_label() || self.try_parse_instruction()?) {
				return Err(AssemblerError {
					message: "unnexpected token",
					span: self.tokens[self.cursor].span,
				});
			}
		}

		self.expect_last_func_closed()?;
		self.compute_jump_offsets()?;

		let mut result: Vec<u8> = Vec::with_capacity(self.machine_code.len() * 2);
		for instruction in self.machine_code {
			let bytes = instruction.to_le_bytes();
			result.push(bytes[1]);
			result.push(bytes[0]);
		}

		Ok(result)
	}

	fn compute_jump_offsets(&mut self) -> Result<(), AssemblerError> {
		for (src, label_span) in self.jmp_src.iter() {
			let (src, label_span) = (*src, *label_span);

			let label = self.source_file.source_slice(label_span);

			let dst = *self.jmp_dst.get(label).ok_or(AssemblerError {
				message: "jump to unknown label",
				span: label_span,
			})?;

			let jump_offset = (dst as isize - src as isize - 1) * 2;

			// check 15-bit signed integer bounds
			if !(-16384..=16383).contains(&jump_offset) {
				return Err(AssemblerError {
					message: "label to jump to is too far",
					span: label_span,
				});
			}

			self.machine_code[src] |= (jump_offset & 0b_01111111_11111110) as u16;
		}

		Ok(())
	}

	fn try_parse_pragma(&mut self) -> Result<bool, AssemblerError> {
		if let Some(tok) = self.accept_tok(TokenKind::Dot) { tok }
		else { return Ok(false); };

		if !(self.try_parse_pragma_push()?
			|| self.try_parse_pragma_pop()?
			|| self.try_parse_pragma_stack_ctx_save()?
			|| self.try_parse_pragma_stack_ctx_restore()?
			|| self.try_parse_pragma_call()?
			|| self.try_parse_pragma_return()?
			|| self.try_parse_pragma_func_start()?
			|| self.try_parse_pragma_func_end()?)
		{
			let pragma_name_tok = self.expect_tok(TokenKind::Identifier)?;
			return Err(AssemblerError {
				message: "unknown pragma",
				span: pragma_name_tok.span,
			});
		}

		self.expect_tok(TokenKind::EndOfLine)?;
		Ok(true)
	}

	fn try_parse_pragma_push(&mut self) -> Result<bool, AssemblerError> {
		if self.accept_tok_exact(TokenKind::Identifier, "push").is_none() {
			return Ok(false);
		};

		let push_instruction = &INSTRUCTIONS["push"];

		let (operand, operand_span) = self.try_parse_operand()?.ok_or_else(|| AssemblerError {
			message: "expected an operand",
			span: self.cursor_span(),
		})?;

		if !push_instruction.allowed_operands.contains(operand.kind) {
			return Err(AssemblerError {
				message: "unexpected type of operand",
				span: operand_span,
			});
		}

		self.expect_tok_exact(TokenKind::Identifier, "as")?;

		if !self.try_parse_local_var_declaration()? {
			return Err(AssemblerError {
				message: "expected variable name",
				span: self.cursor_span(),
			});
		}

		self.machine_code.push(push_instruction.opcode | operand.encoding);
		Ok(true)
	}

	fn try_parse_pragma_pop(&mut self) -> Result<bool, AssemblerError> {
		if self.accept_tok_exact(TokenKind::Identifier, "pop").is_none() {
			return Ok(false);
		};

		let pop_instruction = &INSTRUCTIONS["pop"];

		let var_name_tok = self.expect_tok(TokenKind::Identifier)?;
		let var_name = self.tok_text(&var_name_tok);

		if self.stack_ctx.last().filter(|elem| **elem == var_name).is_none() {
			return Err(AssemblerError {
				message: "given variable is not on top of stack",
				span: var_name_tok.span,
			});
		}

		self.expect_tok_exact(TokenKind::Identifier, "into")?;

		let (operand, operand_span) =
			if let Some(void_tok) = self.accept_tok_exact(TokenKind::Identifier, INTRINSIC_VAR_VOID) {
				(Operand { kind: OperandType::Void, encoding: 0 }, void_tok.span )
			} else {
				self.try_parse_operand()?.ok_or_else(|| AssemblerError {
					message: "expected an operand",
					span: self.cursor_span(),
				})?
			};

		if !pop_instruction.allowed_operands.contains(operand.kind) {
			return Err(AssemblerError {
				message: "unexpected type of operand",
				span: operand_span,
			});
		}

		self.stack_ctx.pop();
		self.machine_code.push(pop_instruction.opcode | operand.encoding);
		Ok(true)
	}

	fn try_parse_pragma_stack_ctx_save(&mut self) -> Result<bool, AssemblerError> {
		if self.accept_tok_exact(TokenKind::Identifier, "stack_ctx_save").is_none() {
			return Ok(false);
		}

		let name_tok = self.expect_tok(TokenKind::Identifier)?;
		let name = self.tok_text(&name_tok);

		if self.stack_ctx.contains(&name) {
			return Err(AssemblerError {
				message: "stack context name already in use",
				span: name_tok.span,
			});
		}

		self.saved_stack_ctxs.insert(name, self.stack_ctx.clone());

		Ok(true)
	}

	fn try_parse_pragma_stack_ctx_restore(&mut self) -> Result<bool, AssemblerError> {
		if self.accept_tok_exact(TokenKind::Identifier, "stack_ctx_restore").is_none() {
			return Ok(false);
		}

		let name_tok = self.expect_tok(TokenKind::Identifier)?;
		let name = self.tok_text(&name_tok);

		self.stack_ctx = self.saved_stack_ctxs.get(&name)
			.ok_or(AssemblerError {
				message: "unknown stack context name",
				span: name_tok.span,
			})?
			.clone();

		Ok(true)
	}

	fn try_parse_pragma_call(&mut self) -> Result<bool, AssemblerError> {
		if self.accept_tok_exact(TokenKind::Identifier, "call").is_none() {
			return Ok(false);
		};

		let label_tok = self.expect_tok(TokenKind::Identifier)?;

		self.machine_code.push(OPCODE_PUSH | REGCODE_HIP | 2);
		self.machine_code.push(OPCODE_PUSH | REGCODE_LIP | 1);
		self.jmp_src.insert(self.machine_code.len(), label_tok.span);
		self.machine_code.push(OPCODE_JMP); // we'll encode the jump offset later

		Ok(true)
	}

	fn try_parse_pragma_return(&mut self) -> Result<bool, AssemblerError> {
		let pragma_name_tok =
			if let Some(tok) = self.accept_tok_exact(TokenKind::Identifier, "return") { tok }
			else { return Ok(false); };

		let func_ctx = self.current_func_ctx.as_ref().ok_or(AssemblerError {
			message: "can't use return outside a function",
			span: pragma_name_tok.span,
		})?;

		if self.stack_ctx.len() < func_ctx.stack_header_size {
			return Err(AssemblerError {
				message: "too much data was popped from the stack before return",
				span: pragma_name_tok.span,
			});
		}

		if self.stack_ctx.len() > func_ctx.stack_header_size {
			return Err(AssemblerError {
				message: "data was pushed to the stack in function body, but was not popped before return",
				span: pragma_name_tok.span,
			});
		}

		self.machine_code.push(OPCODE_POP | REGCODE_LIP); // pop stack value into buffer
		self.machine_code.push(OPCODE_POP | REGCODE_HIP); // pop stack value into HIP, and pop buffer into LIP (atomic 16-bit write)

		Ok(true)
	}

	fn try_parse_pragma_func_start(&mut self) -> Result<bool, AssemblerError> {
		let pragma_name_tok =
			if let Some(tok) = self.accept_tok_exact(TokenKind::Identifier, "func_start") { tok }
			else { return Ok(false); };

		self.expect_last_func_closed()?;

		if !self.stack_ctx.is_empty() {
			return Err(AssemblerError {
				message: "stack context should be empty when declaring a function",
				span: pragma_name_tok.span,
			});
		}

		while self.try_parse_local_var_declaration()? {}

		self.stack_ctx.push(INTRINSIC_VAR_RET_ADDR_L);
		self.stack_ctx.push(INTRINSIC_VAR_RET_ADDR_H);

		self.current_func_ctx = Some(FuncCtx {
			stack_header_size: self.stack_ctx.len(),
			func_start_span: pragma_name_tok.span,
		});

		Ok(true)
	}

	fn try_parse_pragma_func_end(&mut self) -> Result<bool, AssemblerError> {
		let pragma_name_tok =
			if let Some(tok) = self.accept_tok_exact(TokenKind::Identifier, "func_end") { tok }
			else { return Ok(false); };

		let func_ctx = self.current_func_ctx.as_ref().ok_or(AssemblerError {
			message: "can't use func_end outside a function",
			span: pragma_name_tok.span,
		})?;

		if self.stack_ctx.len() < func_ctx.stack_header_size {
			return Err(AssemblerError {
				message: "too much data was popped from the stack in function body",
				span: pragma_name_tok.span,
			});
		}

		if self.stack_ctx.len() > func_ctx.stack_header_size {
			return Err(AssemblerError {
				message: "data was pushed to the stack in function body, but was not popped",
				span: pragma_name_tok.span,
			});
		}

		self.stack_ctx.clear();
		self.current_func_ctx = None;

		Ok(true)
	}

	fn try_parse_local_var_declaration(&mut self) -> Result<bool, AssemblerError> {
		let identifier_tok =
			if let Some(tok) = self.accept_tok(TokenKind::Identifier) { tok }
			else { return Ok(false); };

		let identifier = self.tok_text(&identifier_tok);

		if REGISTERS.get(identifier).is_some()
			|| identifier == INTRINSIC_VAR_VOID
			|| identifier == INTRINSIC_VAR_RET_ADDR_L
			|| identifier == INTRINSIC_VAR_RET_ADDR_H
		{
			return Err(AssemblerError {
				message: "reserved name",
				span: identifier_tok.span,
			});
		}

		if self.stack_ctx.contains(&identifier) {
			return Err(AssemblerError {
				message: "variable name already in use",
				span: identifier_tok.span,
			});
		}

		self.stack_ctx.push(identifier);
		Ok(true)
	}

	fn try_parse_label(&mut self) -> bool {
		let (identifier_tok, _colon_tok) =
			if let Some(tokens) = self.accept_2_tok(TokenKind::Identifier, TokenKind::Colon) { tokens }
			else { return false; };

		self.accept_tok(TokenKind::EndOfLine);

		self.jmp_dst.insert(self.tok_text(&identifier_tok), self.machine_code.len());

		true
	}

	fn try_parse_instruction(&mut self) -> Result<bool, AssemblerError> {
		let mnemonic_tok =
			if let Some(tok) = self.accept_tok(TokenKind::Identifier) { tok }
			else { return Ok(false); };

		let mnemonic = self.tok_text(&mnemonic_tok);

		let instruction = INSTRUCTIONS.get(mnemonic).ok_or(AssemblerError {
			message: "unknown instruction",
			span: mnemonic_tok.span,
		})?;

		let mut instruction_encoding = instruction.opcode;

		if mnemonic == "jmp" || mnemonic == "jnz" {
			let label_tok = self.expect_tok(TokenKind::Identifier)?;
			self.jmp_src.insert(self.machine_code.len(), label_tok.span);

		} else if let Some((operand, operand_span)) = self.try_parse_operand()? {
			if !instruction.allowed_operands.contains(operand.kind) {
				return Err(AssemblerError {
					message: "unexpected type of operand",
					span: operand_span,
				});
			}

			instruction_encoding |= operand.encoding;

		} else if !instruction.allowed_operands.contains(OperandType::Void) {
			return Err(AssemblerError {
				message: "expected an operand",
				span: self.cursor_span(),
			});
		}

		self.expect_tok(TokenKind::EndOfLine)?;

		self.machine_code.push(instruction_encoding);
		Ok(true)
	}

	fn try_parse_operand(&mut self) -> Result<Option<(Operand, Span)>, AssemblerError> {
		let cursor_start = self.cursor;

		let operand =
			if let Some(op) = self.try_parse_operand_imm8()? { Some(op) }
			else if let Some(op) = self.try_parse_operand_mem()? { Some(op) }
			else { self.try_parse_operand_reg()? };

		let span = self.tokens[cursor_start].span.merge(self.tokens[self.cursor - 1].span);

		Ok(operand.map(|op| (op, span)))
	}

	fn try_parse_operand_imm8(&mut self) -> Result<Option<Operand>, AssemblerError> {
		self.try_parse_litteral().map(|option| option.map(|(value, _)| Operand {
			kind: OperandType::Imm,
			encoding: value as u16,
		}))
	}

	fn try_parse_operand_mem(&mut self) -> Result<Option<Operand>, AssemblerError> {
		if self.accept_tok(TokenKind::LBracket).is_none() {
			return Ok(None);
		};

		let identifier_tok = self.expect_tok(TokenKind::Identifier)?;
		let identifier_text = self.tok_text(&identifier_tok);

		let operand_value =
			if identifier_text == "AD" || identifier_text == "SP" {
				let offset =
					if let Some(_plus_tok) = self.accept_tok(TokenKind::Plus) {
						self.try_parse_litteral()?.ok_or_else(|| AssemblerError {
							message: "expected offset",
							span: self.cursor_span(),
						})?.0
					} else {
						0
					};

				let opcode = match identifier_text {
					"AD" => OPERAND_CODE_AD,
					"SP" => OPERAND_CODE_SP,
					_ => panic!(),
				};

				opcode | offset as u16

			} else if let Some(var_idx) = self.stack_ctx.iter().position(|elem| elem == &identifier_text) {
				let offset = self.stack_ctx.len() - var_idx - 1;
				OPERAND_CODE_SP | offset as u16

			} else {
				return Err(AssemblerError {
					message: "expected AD, SP, or variable name",
					span: identifier_tok.span,
				});
			};

		self.expect_tok(TokenKind::RBracket)?;

		Ok(Some(Operand {
			kind: OperandType::Mem,
			encoding: operand_value,
		}))
	}

	fn try_parse_operand_reg(&mut self) -> Result<Option<Operand>, AssemblerError> {
		let register_name_tok =
			if let Some(tok) = self.accept_tok(TokenKind::Identifier) { tok }
			else { return Ok(None); };

		let register_name = self.tok_text(&register_name_tok);

		let mut register_encoding = REGISTERS.get(register_name)
			.ok_or(AssemblerError {
				message: "unknown register name",
				span: register_name_tok.span,
			})?.to_owned();

		if (register_name == "LIP" || register_name == "HIP") && self.accept_tok(TokenKind::Plus).is_some() {
			let (offset, offset_tok) = self.try_parse_litteral()?.ok_or_else(|| AssemblerError {
				message: "expected litteral",
				span: self.cursor_span(),
			})?;

			if offset % 2 != 0 {
				return Err(AssemblerError { message: "instruction pointer offset should be even", span: offset_tok.span });
			}

			if offset > 30 {
				return Err(AssemblerError { message: "instruction pointer offset can't exceed 30", span: offset_tok.span });
			}

			register_encoding |= (offset >> 1) as u16;
		}

		Ok(Some(Operand {
			kind: OperandType::Reg,
			encoding: register_encoding,
		}))
	}

	fn try_parse_litteral(&mut self) -> Result<Option<(u8, Token)>, AssemblerError> {
		let (str_value, radix, token) =
			if let Some(tok) = self.accept_tok(TokenKind::LitteralBin) {
				(&self.tok_text(&tok)[2..], 2, tok)

			} else if let Some(tok) = self.accept_tok(TokenKind::LitteralDec) {
				(self.tok_text(&tok), 10, tok)

			} else if let Some(tok) = self.accept_tok(TokenKind::LitteralHex) {
				(&self.tok_text(&tok)[2..], 16, tok)

			} else {
				return Ok(None);
			};

		match u8::from_str_radix(str_value, radix) {
			Ok(value) => Ok(Some((value, token))),
			Err(err) => {
				assert!(*err.kind() == IntErrorKind::PosOverflow);
				Err(AssemblerError { message: "value greater than 255", span: token.span })
			}
		}
	}

	fn next_tok(&mut self) -> Option<Token> {
		let tok = self.tokens.get(self.cursor).cloned();
		if tok.is_some() {
			self.cursor += 1;
		};
		tok
	}

	fn accept_tok(&mut self, tok_kind: TokenKind) -> Option<Token> {
		let res = self.tokens.get(self.cursor).filter(|tok| tok.kind == tok_kind);
		if res.is_some() {
			self.cursor += 1;
		}
		res.cloned()
	}

	fn accept_tok_exact(&mut self, tok_kind: TokenKind, text: &str) -> Option<Token> {
		let res = self.tokens.get(self.cursor).filter(|tok| tok.kind == tok_kind && self.tok_text(tok) == text);
		if res.is_some() {
			self.cursor += 1;
		}
		res.cloned()
	}

	fn accept_2_tok(&mut self, tok0_kind: TokenKind, tok1_kind: TokenKind ) -> Option<(Token, Token)> {
		let tok0 = self.tokens.get(self.cursor    ).filter(|tok| tok.kind == tok0_kind)?;
		let tok1 = self.tokens.get(self.cursor + 1).filter(|tok| tok.kind == tok1_kind)?;
		self.cursor += 2;
		Some((tok0.clone(), tok1.clone()))
	}

	fn expect_tok(&mut self, tok_kind: TokenKind) -> Result<Token, AssemblerError> {
		let tok = self.next_tok().ok_or_else(|| AssemblerError {
			message: "unexpected end of file",
			span: self.cursor_span(),
		})?;

		if tok.kind == tok_kind {
			Ok(tok)
		} else {
			Err(AssemblerError {
				message: "unexpected token",
				span: tok.span,
			})
		}
	}

	fn expect_tok_exact(&mut self, tok_kind: TokenKind, text: &str ) -> Result<Token, AssemblerError> {
		let tok = self.next_tok().ok_or_else(|| AssemblerError {
			message: "unexpected end of file",
			span: self.cursor_span(),
		})?;

		if tok.kind == tok_kind && self.tok_text(&tok) == text {
			Ok(tok)
		} else {
			Err(AssemblerError {
				message: "unexpected token",
				span: tok.span,
			})
		}
	}

	fn expect_last_func_closed(&self) -> Result<(), AssemblerError> {
		match &self.current_func_ctx {
			None => Ok(()),
			Some(func_ctx) => Err(AssemblerError {
				message: "function was not closed with .func_end",
				span: func_ctx.func_start_span,
			}),
		}
	}

	fn tok_text(&self, tok: &Token) -> &'a str {
		self.source_file.source_slice(tok.span)
	}

	fn cursor_span(&self) -> Span {
		if self.cursor == 0 {
			self.source_file.span.subspan(0, 0)
		} else {
			let prev_tok_span = self.tokens[self.cursor - 1].span;
			prev_tok_span.subspan(prev_tok_span.len(), prev_tok_span.len())
		}
	}
}
