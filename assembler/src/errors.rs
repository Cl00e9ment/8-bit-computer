use std::process;

use codemap::{Span, CodeMap};
use codemap_diagnostic::{Emitter, Diagnostic, Level, ColorConfig, SpanLabel, SpanStyle};

#[derive(Debug)]
pub struct AssemblerError {
	pub message: &'static str,
	pub span: Span,
}

pub fn abort_with_msg(msg: String) -> ! {
	let mut emitter = Emitter::stderr(ColorConfig::Always, None);
	emitter.emit(&[Diagnostic {
		level: Level::Error,
		message: msg,
		code: None,
		spans: vec![],
	}]);
	process::exit(1);
}

pub fn abort_with_asm_err(err: AssemblerError, codemap: &CodeMap) -> ! {
	let label = SpanLabel {
		span: err.span,
		style: SpanStyle::Primary,
		label: None
	};
	let d = Diagnostic {
		level: Level::Error,
		message: err.message.to_owned(),
		code: None,
		spans: vec![label],
	};

	let mut emitter = Emitter::stderr(ColorConfig::Always, Some(codemap));
	emitter.emit(&[d]);
	process::exit(1);
}
