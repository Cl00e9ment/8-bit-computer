#![allow(clippy::unusual_byte_groupings)]

use std::collections::HashMap;

use bitflags::bitflags;
use static_init::dynamic;

bitflags! {
	#[derive(Clone, Copy)]
	pub struct OperandType: u32 {
		/** immediate */
		const Imm     = 0b00000001;
		/** memory location: [AD] or [SP+x] */
		const Mem     = 0b00000010;
		/** register */
		const Reg     = 0b00000100;
		/** offset relative to current IP (so in assembly, a reference to a label) */
		const RelOfst = 0b00001000;
		/** no operand */
		const Void    = 0b00010000;
	}
}

pub struct Instruction {
	pub opcode: u16,
	pub allowed_operands: OperandType,
}

pub static OPCODE_JMP  : u16 = 0b_1_0000000_0000000_1;
pub static OPCODE_JNZ  : u16 = 0b_1_0000000_0000000_0;
pub static OPCODE_ADD  : u16 = 0b_0_00000_00_00000000;
pub static OPCODE_ADDC : u16 = 0b_0_00001_00_00000000;
pub static OPCODE_SUB  : u16 = 0b_0_00010_00_00000000;
pub static OPCODE_SUBB : u16 = 0b_0_00011_00_00000000;
pub static OPCODE_SHL  : u16 = 0b_0_01000_00_00000000;
pub static OPCODE_SHR  : u16 = 0b_0_01010_00_00000000;
pub static OPCODE_SSHR : u16 = 0b_0_01011_00_00000000;
pub static OPCODE_OR   : u16 = 0b_0_10011_00_00000000;
pub static OPCODE_AND  : u16 = 0b_0_10010_00_00000000;
pub static OPCODE_XOR  : u16 = 0b_0_10001_00_00000000;
pub static OPCODE_GTU  : u16 = 0b_0_11000_00_00000000;
pub static OPCODE_LTU  : u16 = 0b_0_11001_00_00000000;
pub static OPCODE_GTS  : u16 = 0b_0_11100_00_00000000;
pub static OPCODE_LTS  : u16 = 0b_0_11101_00_00000000;
pub static OPCODE_EQU  : u16 = 0b_0_11010_00_00000000;
pub static OPCODE_NEQ  : u16 = 0b_0_11011_00_00000000;
pub static OPCODE_LOAD : u16 = 0b_0_00100_00_00000000;
pub static OPCODE_STORE: u16 = 0b_0_00101_00_00000000;
pub static OPCODE_PUSH : u16 = 0b_0_00110_00_00000000;
pub static OPCODE_POP  : u16 = 0b_0_00111_00_00000000;
pub static OPCODE_ALM  : u16 = 0b_0_10100_00_00000000;
pub static OPCODE_IRET : u16 = 0b_0_10101_00_00000000;
pub static OPCODE_DIN  : u16 = 0b_0_10110_00_00000000;
pub static OPCODE_EIN  : u16 = 0b_0_10111_00_00000000;

pub static REGCODE_HIP: u16 = 0b_00000011_00000000;
pub static REGCODE_LIP: u16 = 0b_00000011_00100000;
pub static REGCODE_HAD: u16 = 0b_00000011_01000000;
pub static REGCODE_LAD: u16 = 0b_00000011_01100000;
pub static REGCODE_AC : u16 = 0b_00000011_10000000;

pub static OPERAND_CODE_SP : u16 = 0b_00000001_00000000;
pub static OPERAND_CODE_AD : u16 = 0b_00000010_00000000;

#[dynamic]
pub static INSTRUCTIONS: HashMap<&'static str, Instruction> = HashMap::from([
	("jmp"  , Instruction { opcode: OPCODE_JMP  , allowed_operands: OperandType::RelOfst }),
	("jnz"  , Instruction { opcode: OPCODE_JNZ  , allowed_operands: OperandType::RelOfst }),
	("add"  , Instruction { opcode: OPCODE_ADD  , allowed_operands: OperandType::Imm | OperandType::Reg | OperandType::Mem }),
	("addc" , Instruction { opcode: OPCODE_ADDC , allowed_operands: OperandType::Imm | OperandType::Reg | OperandType::Mem }),
	("sub"  , Instruction { opcode: OPCODE_SUB  , allowed_operands: OperandType::Imm | OperandType::Reg | OperandType::Mem }),
	("subb" , Instruction { opcode: OPCODE_SUBB , allowed_operands: OperandType::Imm | OperandType::Reg | OperandType::Mem }),
	("shl"  , Instruction { opcode: OPCODE_SHL  , allowed_operands: OperandType::Imm | OperandType::Reg | OperandType::Mem }),
	("shr"  , Instruction { opcode: OPCODE_SHR  , allowed_operands: OperandType::Imm | OperandType::Reg | OperandType::Mem }),
	("sshr" , Instruction { opcode: OPCODE_SSHR , allowed_operands: OperandType::Imm | OperandType::Reg | OperandType::Mem }),
	("or"   , Instruction { opcode: OPCODE_OR   , allowed_operands: OperandType::Imm | OperandType::Reg | OperandType::Mem }),
	("and"  , Instruction { opcode: OPCODE_AND  , allowed_operands: OperandType::Imm | OperandType::Reg | OperandType::Mem }),
	("xor"  , Instruction { opcode: OPCODE_XOR  , allowed_operands: OperandType::Imm | OperandType::Reg | OperandType::Mem }),
	("gtu"  , Instruction { opcode: OPCODE_GTU  , allowed_operands: OperandType::Imm | OperandType::Reg | OperandType::Mem }),
	("ltu"  , Instruction { opcode: OPCODE_LTU  , allowed_operands: OperandType::Imm | OperandType::Reg | OperandType::Mem }),
	("gts"  , Instruction { opcode: OPCODE_GTS  , allowed_operands: OperandType::Imm | OperandType::Reg | OperandType::Mem }),
	("lts"  , Instruction { opcode: OPCODE_LTS  , allowed_operands: OperandType::Imm | OperandType::Reg | OperandType::Mem }),
	("equ"  , Instruction { opcode: OPCODE_EQU  , allowed_operands: OperandType::Imm | OperandType::Reg | OperandType::Mem }),
	("neq"  , Instruction { opcode: OPCODE_NEQ  , allowed_operands: OperandType::Imm | OperandType::Reg | OperandType::Mem }),
	("load" , Instruction { opcode: OPCODE_LOAD , allowed_operands: OperandType::Imm | OperandType::Reg | OperandType::Mem }),
	("store", Instruction { opcode: OPCODE_STORE, allowed_operands: OperandType::Reg | OperandType::Mem }),
	("push" , Instruction { opcode: OPCODE_PUSH , allowed_operands: OperandType::Imm | OperandType::Reg }),
	("pop"  , Instruction { opcode: OPCODE_POP  , allowed_operands: OperandType::Reg | OperandType::Void }),
	("alm"  , Instruction { opcode: OPCODE_ALM  , allowed_operands: OperandType::Imm | OperandType::Reg | OperandType::Mem }),
	("iret" , Instruction { opcode: OPCODE_IRET , allowed_operands: OperandType::Void }),
	("din"  , Instruction { opcode: OPCODE_DIN  , allowed_operands: OperandType::Void }),
	("ein"  , Instruction { opcode: OPCODE_EIN  , allowed_operands: OperandType::Void }),

	// just xors with predifined immediates
	("not"  , Instruction { opcode: OPCODE_XOR | 0b_11111111, allowed_operands: OperandType::Void }),
	("bnot" , Instruction { opcode: OPCODE_XOR | 0b_00000001, allowed_operands: OperandType::Void }),
]);

#[dynamic]
pub static REGISTERS: HashMap<&'static str, u16> = HashMap::from([
	("HIP", REGCODE_HIP),
	("LIP", REGCODE_LIP),
	("HAD", REGCODE_HAD),
	("LAD", REGCODE_LAD),
	("AC",  REGCODE_AC ),
]);
