main:
        .push   0 as fibo_result
        .push   10 as n
        .call   fibo
        .pop    n into void
        .pop    fibo_result into AC

        .push   0 as char1
        .push   0 as char0
        .push   AC as fibo_result
        .call   byte_to_ascii_hex
        .pop    fibo_result into void

        load    0x80
        store   HAD
        load    0
        store   LAD

        load    0x30                            ; '0' char
        store   [AD]                            ; send to serial port
        load    0x78                            ; 'x' char
        store   [AD]                            ; send to serial port
        .pop    char0 into AC                   ; first nibble
        store   [AD]                            ; send to serial port
        .pop    char1 into AC                   ; second nibble
        store   [AD]                            ; send to serial port
        load    0x0a                            ; new line char
        store   [AD]                            ; send to serial port

        ; terminate the emulator (emulator-only feature)
        load    1
        store   [AD + 0xff]
        ; or loop infinitely
end:    jmp     end

fibo:
.func_start retval n
        load    [n]
        gtu     1
        jnz     general_case
        load    [n]
        store   [retval]
        .return
general_case:
        .push   0 as fibo_n_minus_1
        load    [n]
        sub     1
        .push   AC as prev_idx
        .call   fibo
        .pop    prev_idx into AC

        .push   0 as fibo_n_minus_2
        sub     1
        .push   AC as prev_idx
        .call   fibo
        .pop    prev_idx into void

        .pop    fibo_n_minus_2 into AC
        add     [fibo_n_minus_1]
        .pop    fibo_n_minus_1 into void
        store   [retval]
        .return
.func_end

byte_to_ascii_hex:
.func_start char1 char0 byte
        load    [byte]
        shr     4
        .push   AC as nibble0
        gtu     9
        .stack_ctx_save nibble0_ifelse
        jnz     letter0
        .pop    nibble0 into AC
        add     48
        jmp     nibble0_end
letter0:
        .stack_ctx_restore nibble0_ifelse
        .pop    nibble0 into AC
        add     87
nibble0_end:
        store   [char0]
        load    [byte]
        and     0x0f
        .push   AC as nibble1
        gtu     9
        .stack_ctx_save nibble1_ifelse
        jnz     letter1
        .pop    nibble1 into AC
        add     48
        jmp     nibble1_end
letter1:
        .stack_ctx_restore nibble1_ifelse
        .pop    nibble1 into AC
        add     87
nibble1_end:
        store   [char1]
        .return
.func_end
