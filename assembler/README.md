# Assembler program for CompCraft 1 assembly language

## How to use?

1. [install Rust](https://www.rust-lang.org/tools/install)
2. add `$USER/.cargo/bin` to your `$PATH`
3. compile & install assembler: `cargo install --path .`
4. run it: `compcraft-asm fibo-example.asm`

## How to develop?

1. [install Rust](https://www.rust-lang.org/tools/install)
2. modify sources
3. test it: `cargo run fibo-example.asm`

## TODO

- stackframe header is [retvals, args, retaddr] but [retvals, retaddr, args] may be better
- add optimization flag -Os (optimization example: jmp to the nearest return instead of always inserting the 3 instructions)
