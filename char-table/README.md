# Char table

Character table sprite to be loaded into the VGA EEPROM.

- `char-table.xcf`: source (GIMP file)
- `char-table.png`: exported image
- `encode-img.py`: script to transform the image into an C header file (located at `../eeprom-flasher/include/char-table.h`)
