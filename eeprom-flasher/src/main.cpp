#include <Arduino.h>
#include "char-table.h"

/*
 * PIN CORRESPONDANCE
 *
 * data bus:
 * ATMEGA2656:  PA0  PA1  PA2  PA3  PA4  PA5  PA6  PA7
 * AT28C64B  : I/O0 I/O1 I/O2 I/O3 I/O4 I/O5 I/O6 I/O7
 *
 * page address bus:
 * ATMEGA2656:  PL0  PL1  PL2  PL3  PL4  PL5  PL6
 * AT28C64B  :   A6   A7   A8   A9  A10  A11  A12
 *
 * byte address bus:
 * ATMEGA2656:  PC0  PC1  PC2  PC3  PC4  PC5
 * AT28C64B  :   A0   A1   A2   A3   A4   A5
 *
 * control pins:
 * ATMEGA2656:  PG0  PG1  PG2
 * AT28C64B  :  ~CE  ~OE  ~WE
 *
 */

#define BAUD_RATE 9600

#define DATA_BUS_IN   PINA
#define DATA_BUS_OUT  PORTA
#define PAGE_ADDR_BUS PORTL
#define BYTE_ADDR_BUS PORTC

#define DATA_BUS_SET_MODE_READ  (DDRA = 0b00000000)
#define DATA_BUS_SET_MODE_WRITE (DDRA = 0b11111111)

#define INIT_CONTROL            (DDRG = 0b00000111)
#define INIT_PAGE_ADDR_BUS      (DDRL = 0b01111111)
#define INIT_BYTE_ADDR_BUS      (DDRC = 0b00111111)

#define ENABLE_CHIP    (PORTG &= 0b11111110)
#define DISABLE_CHIP   (PORTG |= 0b00000001)

#define ENABLE_OUTPUT  (PORTG &= 0b11111101)
#define DISABLE_OUTPUT (PORTG |= 0b00000010)

#define ENABLE_WRITE   (PORTG &= 0b11111011)
#define DISABLE_WRITE  (PORTG |= 0b00000100)

#define DISABLE_ALL    (PORTG |= 0b00000111)

/**
 * <b>pre-conditions:</b>
 *
 * <pre>
 * chip:          enabled<br>
 * output:        disabled<br>
 * write:         enabled or disabled<br>
 * data bus mode: write
 * </pre>
 *
 * <b>post-conditions:</b>
 *
 * <pre>
 * chip:          enabled<br>
 * output:        disabled<br>
 * write:         disabled<br>
 * data bus mode: write
 * </pre>
 */
void write_page(uint8_t page_addr, const uint8_t page[PAGE_SIZE]) {
	PAGE_ADDR_BUS = page_addr;
	bool written = false;

	while (!written) {
		// write data
		for (uint8_t i = 0; i < PAGE_SIZE; ++i) {
			BYTE_ADDR_BUS = i;
			ENABLE_WRITE;
			DATA_BUS_OUT = page[i];
			DISABLE_WRITE;
		}

		// wait end of write cycle
		DATA_BUS_SET_MODE_READ;
		ENABLE_OUTPUT;
		while (DATA_BUS_IN != page[PAGE_SIZE - 1]);
		written = true;

		// verify
		for (uint8_t i = 0; i < PAGE_SIZE; ++i) {
			BYTE_ADDR_BUS = i;
			// FIXME find real delay to wait in doc
			delayMicroseconds(2);
			if (DATA_BUS_IN != page[i]) {
				written = false;
				break;
			}
		}

		// reset to write mode
		DISABLE_OUTPUT;
		DATA_BUS_SET_MODE_WRITE;
	}
}

void setup() {
	// When programming the Arduino, it can start the program and then quickly reset.
	// Passed the delay bellow, the program will probably not be interrupted by a reset.
	delay(2000);

	Serial.begin(BAUD_RATE);

	INIT_CONTROL;
	DISABLE_ALL;

	INIT_PAGE_ADDR_BUS;
	INIT_BYTE_ADDR_BUS;

	// write only first charset because the Arduino don't have enough space to store the whole content of the EEPROM
	Serial.print("writing first charset to EEPROM...\n");
	ENABLE_CHIP;
	DATA_BUS_SET_MODE_WRITE;
	for (uint8_t i = 0; i < NB_OF_PAGE / 2; ++i) {
		write_page(i, pages[i]);
	}

	// Now that the first charset is written to the EEPROM,
	// we can re-use the space on the Arduino to generate and store the second charset before writing it to the EEPROM.
	Serial.print("generating second charset...\n");
	for (uint8_t pixelLine = 0; pixelLine < 16; ++pixelLine) {
		for (uint8_t charIdx = 0; true; ++charIdx) {
			uint8_t byte = charIdx >> (pixelLine / 4 * 2);

			if (byte & 0b00000010) {
				byte |= 0b11110000;
			} else {
				byte &= 0b00001111;
			}

			if (byte & 0b00000001) {
				byte |= 0b00001111;
			} else {
				byte &= 0b11110000;
			}

			uint8_t pageAddr = (pixelLine << 2) | (charIdx >> 6);
			uint8_t byteAddr = charIdx & 0b00111111;
			pages[pageAddr][byteAddr] = byte;

			// manually check for the last value to avoid rollover
			if (charIdx == 255) {
				break;
			}
		}
	}

	Serial.print("writing second charset to EEPROM...\n");
	for (uint8_t i = 0; i < NB_OF_PAGE / 2; ++i) {
		write_page(NB_OF_PAGE / 2 + i, pages[i]);
	}

	DISABLE_CHIP;
	DATA_BUS_SET_MODE_READ;

	Serial.print("done!\n");

	Serial.flush();
	exit(0);
}

void loop() {
	__builtin_unreachable();
}
