# Unit tests

Different unit tests to individually test the different modules of the computer.

They are hardware unit tests, so they require to be loaded into an Arduino Mega and to connect each wire to the correct pin.
