#include <Arduino.h>

#include "io.hpp"
#include "tests.h"

void setup(void) {
	io_init();

	Serial.println("starting tests");
	tests();
	Serial.println("tests finished");
}

void loop(void) {}
