#include <Arduino.h>

#include "io.hpp"

void io_init() {
	// DUT inputs (Arduino outputs)
	DDRC = 0xff;
	DDRK = 0b00111111;

	// DUT outputs (Arduino inputs)
	DDRA = 0x00;
	DDRL = 0x00;
	DDRF = 0x00;
	DDRB = 0x00;

	Serial.begin(9600);
}

void do_operation(const struct inputs_t *inputs, struct outputs_t *outputs) {
	auto *input_buf = (uint8_t *) inputs;
	auto *output_buf = (uint8_t *) outputs;

	PORTC = input_buf[0];
	PORTK = input_buf[1];

	asm volatile ("nop");
	asm volatile ("nop");

	output_buf[0] = PINA;
	output_buf[1] = PINL;
	output_buf[2] = PINF;
	output_buf[3] = PINB;
}

void check_field(const char *field, uint8_t expected, uint8_t actual) {
	if (expected == actual) {
		return;
	}

	Serial.print("error: expected ");
	Serial.print(field);
	Serial.print(" to be ");
	Serial.print(expected, DEC);
	Serial.print(" but got ");
	Serial.print(actual, DEC);
	Serial.print("\r\n");
}

void check_outputs(const struct outputs_t *expected, const struct outputs_t *actual) {
	check_field("AC_load_from_ALU"   , expected->AC_load_from_ALU,    actual->AC_load_from_ALU);
	check_field("SP_EN_increment"    , expected->SP_EN_increment,     actual->SP_EN_increment);
	check_field("SP_EN_decrement"    , expected->SP_EN_decrement,     actual->SP_EN_decrement);
	check_field("not_mem_OE"         , expected->not_mem_OE,          actual->not_mem_OE);
	check_field("mem_WE"             , expected->mem_WE,              actual->mem_WE);
	check_field("not_instr_data_OE"  , expected->not_instr_data_OE,   actual->not_instr_data_OE);
	check_field("not_instr_OE"       , expected->not_instr_OE,        actual->not_instr_OE);
	check_field("IP_relative_loading", expected->IP_relative_loading, actual->IP_relative_loading);
	check_field("IP_WE"              , expected->IP_WE,               actual->IP_WE);
	check_field("IP_lsb_buf_WE"      , expected->IP_lsb_buf_WE,       actual->IP_lsb_buf_WE);
	check_field("IP_select_data"     , expected->IP_select_data,      actual->IP_select_data);
	check_field("HAD_WE"             , expected->HAD_WE,              actual->HAD_WE);
	check_field("LAD_WE"             , expected->LAD_WE,              actual->LAD_WE);
	check_field("AC_WE"              , expected->AC_WE,               actual->AC_WE);
	check_field("enable_interrupt"   , expected->enable_interrupt,    actual->enable_interrupt);
	check_field("disable_interrupt"  , expected->disable_interrupt,   actual->disable_interrupt);
	check_field("not_addr_ofst_OE"   , expected->not_addr_ofst_OE,    actual->not_addr_ofst_OE);
	check_field("not_SP_OE"          , expected->not_SP_OE,           actual->not_SP_OE);
	check_field("not_AD_OE"          , expected->not_AD_OE,           actual->not_AD_OE);
	check_field("not_HIP_OE"         , expected->not_HIP_OE,          actual->not_HIP_OE);
	check_field("not_LIP_OE"         , expected->not_LIP_OE,          actual->not_LIP_OE);
	check_field("not_HAD_OE"         , expected->not_HAD_OE,          actual->not_HAD_OE);
	check_field("not_LAD_OE"         , expected->not_LAD_OE,          actual->not_LAD_OE);
	check_field("not_AC_OE"          , expected->not_AC_OE,           actual->not_AC_OE);
	check_field("not_IRA_OE"         , expected->not_IRA_OE,          actual->not_IRA_OE);
	check_field("ALM_write"          , expected->ALM_write,           actual->ALM_write);
}
