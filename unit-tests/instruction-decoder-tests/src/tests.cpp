#include "io.hpp"
#include "tests.h"

void test_jmp() {
	struct inputs_t inputs = {
		.control             = 0b10000000, // jmp
		.instr_data7_5_and_0 = 0b0001,     //
		.clk7_14             = 1,
		.AC0                 = 0,
	};

	struct outputs_t outputs_expected = OUTPUTS_DEFAULT;
	outputs_expected.not_instr_OE = 0;
	outputs_expected.IP_relative_loading = 1;
	outputs_expected.IP_WE = 1;

	struct outputs_t outputs_actual = {};
	do_operation(&inputs, &outputs_actual);

	check_outputs(&outputs_expected, &outputs_actual);
}

void test_jnz_0() {
	struct inputs_t inputs = {
		.control             = 0b10000000, // jnz
		.instr_data7_5_and_0 = 0b0000,     //
		.clk7_14             = 1,
		.AC0                 = 0,
	};

	struct outputs_t outputs_actual = {};
	do_operation(&inputs, &outputs_actual);

	check_outputs(&OUTPUTS_DEFAULT, &outputs_actual);
}

void test_jnz_1() {
	struct inputs_t inputs = {
		.control             = 0b10000000, // jnz
		.instr_data7_5_and_0 = 0b0000,     //
		.clk7_14             = 1,
		.AC0                 = 1,
	};

	struct outputs_t outputs_expected = OUTPUTS_DEFAULT;
	outputs_expected.not_instr_OE = 0;
	outputs_expected.IP_relative_loading = 1;
	outputs_expected.IP_WE = 1;

	struct outputs_t outputs_actual = {};
	do_operation(&inputs, &outputs_actual);

	check_outputs(&outputs_expected, &outputs_actual);
}

void test_alu_immediate() {
	struct inputs_t inputs = {
		.control             = 0b00000000, // add immediate
		.instr_data7_5_and_0 = 0b1110,     // 224
		.clk7_14             = 1,
		.AC0                 = 0,
	};

	struct outputs_t outputs_expected = OUTPUTS_DEFAULT;
	outputs_expected.not_instr_data_OE = 0;
	outputs_expected.AC_load_from_ALU = 1;
	outputs_expected.AC_WE = 1;

	struct outputs_t outputs_actual = {};
	do_operation(&inputs, &outputs_actual);

	check_outputs(&outputs_expected, &outputs_actual);
}

void test_alu_indirect_sp() {
	struct inputs_t inputs = {
		.control             = 0b00000001, // add [SP + ...]
		.instr_data7_5_and_0 = 0b0000,     // 224
		.clk7_14             = 1,
		.AC0                 = 0,
	};

	struct outputs_t outputs_expected = OUTPUTS_DEFAULT;
	outputs_expected.not_SP_OE = 0;
	outputs_expected.not_addr_ofst_OE = 0;
	outputs_expected.not_mem_OE = 0;
	outputs_expected.AC_load_from_ALU = 1;
	outputs_expected.AC_WE = 1;

	struct outputs_t outputs_actual = {};
	do_operation(&inputs, &outputs_actual);

	check_outputs(&outputs_expected, &outputs_actual);
}

void test_alu_indirect_ad() {
	struct inputs_t inputs = {
		.control             = 0b00000010, // add [AD + ...]
		.instr_data7_5_and_0 = 0b1110,     // 224
		.clk7_14             = 1,
		.AC0                 = 0,
	};

	struct outputs_t outputs_expected = OUTPUTS_DEFAULT;
	outputs_expected.not_AD_OE = 0;
	outputs_expected.not_addr_ofst_OE = 0;
	outputs_expected.not_mem_OE = 0;
	outputs_expected.AC_load_from_ALU = 1;
	outputs_expected.AC_WE = 1;

	struct outputs_t outputs_actual = {};
	do_operation(&inputs, &outputs_actual);

	check_outputs(&outputs_expected, &outputs_actual);
}

void test_alu_register_hip() {
	struct inputs_t inputs = {
		.control             = 0b00000011, // add H[IP + 2 * ...]
		.instr_data7_5_and_0 = 0b0000,     // 224
		.clk7_14             = 1,
		.AC0                 = 0,
	};

	struct outputs_t outputs_expected = OUTPUTS_DEFAULT;
	outputs_expected.not_HIP_OE = 0;
	outputs_expected.AC_load_from_ALU = 1;
	outputs_expected.AC_WE = 1;

	struct outputs_t outputs_actual = {};
	do_operation(&inputs, &outputs_actual);

	check_outputs(&outputs_expected, &outputs_actual);
}

void test_alu_register_lip() {
	struct inputs_t inputs = {
		.control             = 0b00000011, // add L[IP + 2 * ...]
		.instr_data7_5_and_0 = 0b0010,     // 224
		.clk7_14             = 1,
		.AC0                 = 0,
	};

	struct outputs_t outputs_expected = OUTPUTS_DEFAULT;
	outputs_expected.not_LIP_OE = 0;
	outputs_expected.AC_load_from_ALU = 1;
	outputs_expected.AC_WE = 1;

	struct outputs_t outputs_actual = {};
	do_operation(&inputs, &outputs_actual);

	check_outputs(&outputs_expected, &outputs_actual);
}

void test_alu_register_had() {
	struct inputs_t inputs = {
		.control             = 0b00000011, // add HAD
		.instr_data7_5_and_0 = 0b0100,     // 224
		.clk7_14             = 1,
		.AC0                 = 0,
	};

	struct outputs_t outputs_expected = OUTPUTS_DEFAULT;
	outputs_expected.not_HAD_OE = 0;
	outputs_expected.AC_load_from_ALU = 1;
	outputs_expected.AC_WE = 1;

	struct outputs_t outputs_actual = {};
	do_operation(&inputs, &outputs_actual);

	check_outputs(&outputs_expected, &outputs_actual);
}

void test_alu_register_lad() {
	struct inputs_t inputs = {
		.control             = 0b00000011, // add LAD
		.instr_data7_5_and_0 = 0b0110,     // 224
		.clk7_14             = 1,
		.AC0                 = 0,
	};

	struct outputs_t outputs_expected = OUTPUTS_DEFAULT;
	outputs_expected.not_LAD_OE = 0;
	outputs_expected.AC_load_from_ALU = 1;
	outputs_expected.AC_WE = 1;

	struct outputs_t outputs_actual = {};
	do_operation(&inputs, &outputs_actual);

	check_outputs(&outputs_expected, &outputs_actual);
}

void test_alu_register_ac() {
	struct inputs_t inputs = {
		.control             = 0b00000011, // add
		.instr_data7_5_and_0 = 0b1000,     // AC
		.clk7_14             = 1,
		.AC0                 = 0,
	};

	struct outputs_t outputs_expected = OUTPUTS_DEFAULT;
	outputs_expected.not_AC_OE = 0;
	outputs_expected.AC_load_from_ALU = 1;
	outputs_expected.AC_WE = 1;

	struct outputs_t outputs_actual = {};
	do_operation(&inputs, &outputs_actual);

	check_outputs(&outputs_expected, &outputs_actual);
}

void test_load_immediate() {
	struct inputs_t inputs = {
		.control             = 0b00010000, // load immediate
		.instr_data7_5_and_0 = 0b1110,     // 224
		.clk7_14             = 1,
		.AC0                 = 0,
	};

	struct outputs_t outputs_expected = OUTPUTS_DEFAULT;
	outputs_expected.not_instr_data_OE = 0;
	outputs_expected.AC_WE = 1;

	struct outputs_t outputs_actual = {};
	do_operation(&inputs, &outputs_actual);

	check_outputs(&outputs_expected, &outputs_actual);
}

void test_load_register() {
	struct inputs_t inputs = {
		.control             = 0b00010011, // load LAD
		.instr_data7_5_and_0 = 0b0110,     // 224
		.clk7_14             = 1,
		.AC0                 = 0,
	};

	struct outputs_t outputs_expected = OUTPUTS_DEFAULT;
	outputs_expected.not_LAD_OE = 0;
	outputs_expected.AC_WE = 1;

	struct outputs_t outputs_actual = {};
	do_operation(&inputs, &outputs_actual);

	check_outputs(&outputs_expected, &outputs_actual);
}

void test_load_indirect() {
	struct inputs_t inputs = {
		.control             = 0b00010010, // load [AD + ...]
		.instr_data7_5_and_0 = 0b1110,     // 224
		.clk7_14             = 1,
		.AC0                 = 0,
	};

	struct outputs_t outputs_expected = OUTPUTS_DEFAULT;
	outputs_expected.not_AD_OE = 0;
	outputs_expected.not_addr_ofst_OE = 0;
	outputs_expected.not_mem_OE = 0;
	outputs_expected.AC_WE = 1;

	struct outputs_t outputs_actual = {};
	do_operation(&inputs, &outputs_actual);

	check_outputs(&outputs_expected, &outputs_actual);
}

void test_store_register() {
	struct inputs_t inputs = {
		.control             = 0b00010111, // store LAD
		.instr_data7_5_and_0 = 0b0110,     // 224
		.clk7_14             = 1,
		.AC0                 = 0,
	};

	struct outputs_t outputs_expected = OUTPUTS_DEFAULT;
	outputs_expected.not_AC_OE = 0;
	outputs_expected.LAD_WE = 1;

	struct outputs_t outputs_actual = {};
	do_operation(&inputs, &outputs_actual);

	check_outputs(&outputs_expected, &outputs_actual);
}

void test_store_indirect() {
	struct inputs_t inputs = {
		.control             = 0b00010110, // store [AD + ...]
		.instr_data7_5_and_0 = 0b1110,     // 224
		.clk7_14             = 1,
		.AC0                 = 0,
	};

	struct outputs_t outputs_expected = OUTPUTS_DEFAULT;
	outputs_expected.not_AD_OE = 0;
	outputs_expected.not_addr_ofst_OE = 0;
	outputs_expected.not_AC_OE = 0;
	outputs_expected.mem_WE = 1;

	struct outputs_t outputs_actual = {};
	do_operation(&inputs, &outputs_actual);

	check_outputs(&outputs_expected, &outputs_actual);
}

void test_push_immediate() {
	struct inputs_t inputs = {
		.control             = 0b00011000, // push immediate
		.instr_data7_5_and_0 = 0b1110,     // 224
		.clk7_14             = 1,
		.AC0                 = 0,
	};

	struct outputs_t outputs_expected = OUTPUTS_DEFAULT;
	outputs_expected.not_SP_OE = 0;
	outputs_expected.not_instr_data_OE = 0;
	outputs_expected.mem_WE = 1;
	outputs_expected.SP_EN_decrement = 1;

	struct outputs_t outputs_actual = {};
	do_operation(&inputs, &outputs_actual);

	check_outputs(&outputs_expected, &outputs_actual);
}

void test_push_register() {
	struct inputs_t inputs = {
		.control             = 0b00011011, // push LAD
		.instr_data7_5_and_0 = 0b0110,     // 224
		.clk7_14             = 1,
		.AC0                 = 0,
	};

	struct outputs_t outputs_expected = OUTPUTS_DEFAULT;
	outputs_expected.not_SP_OE = 0;
	outputs_expected.not_LAD_OE = 0;
	outputs_expected.mem_WE = 1;
	outputs_expected.SP_EN_decrement = 1;

	struct outputs_t outputs_actual = {};
	do_operation(&inputs, &outputs_actual);

	check_outputs(&outputs_expected, &outputs_actual);
}

void test_pop_register() {
	struct inputs_t inputs = {
		.control             = 0b00011111, // pop LAD
		.instr_data7_5_and_0 = 0b0110,     // 224
		.clk7_14             = 1,
		.AC0                 = 0,
	};

	struct outputs_t outputs_expected = OUTPUTS_DEFAULT;
	outputs_expected.not_SP_OE = 0;
	outputs_expected.not_mem_OE = 0;
	outputs_expected.LAD_WE = 1;
	outputs_expected.SP_EN_increment = 1;

	struct outputs_t outputs_actual = {};
	do_operation(&inputs, &outputs_actual);

	check_outputs(&outputs_expected, &outputs_actual);
}

void test_pop_void() {
	struct inputs_t inputs = {
		.control             = 0b00011100, // pop void
		.instr_data7_5_and_0 = 0b0000,     // 224
		.clk7_14             = 1,
		.AC0                 = 0,
	};

	struct outputs_t outputs_expected = OUTPUTS_DEFAULT;
	outputs_expected.not_SP_OE = 0;
	outputs_expected.not_mem_OE = 0;
	outputs_expected.SP_EN_increment = 1;

	struct outputs_t outputs_actual = {};
	do_operation(&inputs, &outputs_actual);

	check_outputs(&outputs_expected, &outputs_actual);
}

void test_alm_immediate() {
	struct inputs_t inputs = {
		.control             = 0b01010000, // alm immediate
		.instr_data7_5_and_0 = 0b1110,     // 224
		.clk7_14             = 1,
		.AC0                 = 0,
	};

	struct outputs_t outputs_expected = OUTPUTS_DEFAULT;
	outputs_expected.not_instr_data_OE = 0;
	outputs_expected.ALM_write = 1;

	struct outputs_t outputs_actual = {};
	do_operation(&inputs, &outputs_actual);

	check_outputs(&outputs_expected, &outputs_actual);
}

void test_iret() {
	struct inputs_t inputs = {
		.control             = 0b01010100, // iret
		.instr_data7_5_and_0 = 0b0000,     // 224
		.clk7_14             = 1,
		.AC0                 = 0,
	};

	struct outputs_t outputs_expected = OUTPUTS_DEFAULT;
	outputs_expected.not_IRA_OE = 0;
	outputs_expected.IP_WE = 1;
	outputs_expected.enable_interrupt = 1;

	struct outputs_t outputs_actual = {};
	do_operation(&inputs, &outputs_actual);

	check_outputs(&outputs_expected, &outputs_actual);
}

void test_din() {
	struct inputs_t inputs = {
		.control             = 0b01011000, // din
		.instr_data7_5_and_0 = 0b0000,     // 224
		.clk7_14             = 1,
		.AC0                 = 0,
	};

	struct outputs_t outputs_expected = OUTPUTS_DEFAULT;
	outputs_expected.disable_interrupt = 1;

	struct outputs_t outputs_actual = {};
	do_operation(&inputs, &outputs_actual);

	check_outputs(&outputs_expected, &outputs_actual);
}

void test_ein() {
	struct inputs_t inputs = {
		.control             = 0b01011100, // ein
		.instr_data7_5_and_0 = 0b0000,     // 224
		.clk7_14             = 1,
		.AC0                 = 0,
	};

	struct outputs_t outputs_expected = OUTPUTS_DEFAULT;
	outputs_expected.enable_interrupt = 1;

	struct outputs_t outputs_actual = {};
	do_operation(&inputs, &outputs_actual);

	check_outputs(&outputs_expected, &outputs_actual);
}

void tests() {
	test_jmp();
	test_jnz_0();
	test_jnz_1();
	test_alu_immediate();
	test_alu_indirect_sp();
	test_alu_indirect_ad();
	test_alu_register_hip();
	test_alu_register_lip();
	test_alu_register_had();
	test_alu_register_lad();
	test_alu_register_ac();
	test_load_immediate();
	test_load_register();
	test_load_indirect();
	test_store_register();
	test_store_indirect();
	test_push_immediate();
	test_push_register();
	test_pop_register();
	test_pop_void();
	test_alm_immediate();
	test_iret();
	test_din();
	test_ein();
}
