#pragma once

#include <avr/io.h>

struct inputs_t {
	// PORTC
	uint8_t control;

	// PORTK 0..5
	uint8_t instr_data7_5_and_0: 4; // PORTK 3210
	uint8_t clk7_14            : 1; // PORTK 4
	uint8_t AC0                : 1; // PORTK 5
};

struct outputs_t {
	// PORTA 0..7
	uint8_t AC_load_from_ALU   : 1;
	uint8_t SP_EN_increment    : 1;
	uint8_t SP_EN_decrement    : 1;
	uint8_t not_mem_OE         : 1;
	uint8_t mem_WE             : 1;
	uint8_t not_instr_data_OE  : 1;
	uint8_t not_instr_OE       : 1;
	uint8_t IP_relative_loading: 1;

	// PORTL 0..7
	uint8_t IP_WE              : 1;
	uint8_t IP_lsb_buf_WE      : 1;
	uint8_t IP_select_data     : 1;
	uint8_t HAD_WE             : 1;
	uint8_t LAD_WE             : 1;
	uint8_t AC_WE              : 1;
	uint8_t enable_interrupt   : 1;
	uint8_t disable_interrupt  : 1;

	// PORTF 0..7
	uint8_t not_addr_ofst_OE   : 1;
	uint8_t not_SP_OE          : 1;
	uint8_t not_AD_OE          : 1;
	uint8_t not_HIP_OE         : 1;
	uint8_t not_LIP_OE         : 1;
	uint8_t not_HAD_OE         : 1;
	uint8_t not_LAD_OE         : 1;
	uint8_t not_AC_OE          : 1;

	// PORTB 0..1
	uint8_t not_IRA_OE         : 1;
	uint8_t ALM_write          : 1;
};

const struct outputs_t OUTPUTS_DEFAULT = {
	.AC_load_from_ALU    = 0,
	.SP_EN_increment     = 0,
	.SP_EN_decrement     = 0,
	.not_mem_OE          = 1,
	.mem_WE              = 0,
	.not_instr_data_OE   = 1,
	.not_instr_OE        = 1,
	.IP_relative_loading = 0,
	.IP_WE               = 0,
	.IP_lsb_buf_WE       = 0,
	.IP_select_data      = 0,
	.HAD_WE              = 0,
	.LAD_WE              = 0,
	.AC_WE               = 0,
	.enable_interrupt    = 0,
	.disable_interrupt   = 0,
	.not_addr_ofst_OE    = 1,
	.not_SP_OE           = 1,
	.not_AD_OE           = 1,
	.not_HIP_OE          = 1,
	.not_LIP_OE          = 1,
	.not_HAD_OE          = 1,
	.not_LAD_OE          = 1,
	.not_AC_OE           = 1,
	.not_IRA_OE          = 1,
	.ALM_write           = 0,
};

void io_init();
void do_operation(const struct inputs_t *inputs, struct outputs_t *outputs);
void check_outputs(const struct outputs_t *expected, const struct outputs_t *actual);
