# modify this variable to point to your Arduino install directory
ARDUINO_DIR ?= $(HOME)/.arduino15/packages/arduino/hardware/avr/1.8.6

PORT ?= /dev/ttyACM0

ARDUINO_CORE_DIR = $(ARDUINO_DIR)/cores/arduino
ARDUINO_INCLUDES = -I$(ARDUINO_CORE_DIR) -I$(ARDUINO_DIR)/variants/mega
MCU              = atmega2560
BOARD_FLAGS      = -mmcu=$(MCU) -DF_CPU=16000000L

SRC_DIR     = $(CURDIR)/src
INCLUDE_DIR = $(CURDIR)/include
BUILD_DIR   = $(CURDIR)/build

SOURCES = $(wildcard $(SRC_DIR)/*.cpp)
HEADERS = $(wildcard $(INCLUDE_DIR)/*.hpp)
OBJECTS = $(SOURCES:$(SRC_DIR)/%.cpp=$(BUILD_DIR)/%.o)

CC  = avr-gcc
CXX = avr-g++
AR  = avr-ar

CXXFLAGS  = $(BOARD_FLAGS)
CXXFLAGS += -I$(INCLUDE_DIR) $(ARDUINO_INCLUDES)
CXXFLAGS += -std=gnu++11
CXXFLAGS += -ffunction-sections -fdata-sections -fpermissive -fno-exceptions -fno-threadsafe-statics -Wno-error=narrowing
CXXFLAGS += -Wall -Wextra
CXXFLAGS += -Os
CXXFLAGS += -Wno-array-bounds # to avoid compiler bug (see https://gcc.gnu.org/bugzilla/show_bug.cgi?id=104828)

.PHONY: all build upload clean

all: build

build: $(BUILD_DIR)/firmware.elf

upload:
	avrdude -p $(MCU) -P $(PORT) -c wiring -D -U flash:w:$(BUILD_DIR)/firmware.elf

serial_console:
	picocom $(PORT)

clean:
	rm -rf $(BUILD_DIR)

$(BUILD_DIR)/firmware.elf: $(OBJECTS) $(BUILD_DIR)/arduino/arduinolib.a
	$(CC) -Os -Wl,--gc-sections -mmcu=$(MCU) -o $@ $^ -lm

$(BUILD_DIR)/%.o: $(SRC_DIR)/%.cpp $(HEADERS) | $(BUILD_DIR)
	$(CXX) $(CXXFLAGS) -c -o $@ $<

$(BUILD_DIR):
	mkdir -p $@


# Arduino lib compilation

ARDUINO_SOURCES_C   = $(wildcard $(ARDUINO_CORE_DIR)/*.c)
ARDUINO_SOURCES_CPP = $(wildcard $(ARDUINO_CORE_DIR)/*.cpp)
ARDUINO_OBJECTS_C   = $(ARDUINO_SOURCES_C:$(ARDUINO_CORE_DIR)/%.c=$(BUILD_DIR)/arduino/c/%.o)
ARDUINO_OBJECTS_CPP = $(ARDUINO_SOURCES_CPP:$(ARDUINO_CORE_DIR)/%.cpp=$(BUILD_DIR)/arduino/cpp/%.o)

ARDUINO_CFLAGS   = $(ARDUINO_INCLUDES) $(BOARD_FLAGS) -std=gnu11   -Os -ffunction-sections -fdata-sections
ARDUINO_CXXFLAGS = $(ARDUINO_INCLUDES) $(BOARD_FLAGS) -std=gnu++11 -Os -ffunction-sections -fdata-sections -fpermissive -fno-exceptions -fno-threadsafe-statics -Wno-error=narrowing

$(BUILD_DIR)/arduino/arduinolib.a: $(ARDUINO_OBJECTS_C) $(ARDUINO_OBJECTS_CPP)
	$(AR) -rcs $@ $^

$(BUILD_DIR)/arduino/c/%.o: $(ARDUINO_CORE_DIR)/%.c | $(BUILD_DIR)/arduino/c
	$(CC) $(ARDUINO_CFLAGS) -c -o $@ $^

$(BUILD_DIR)/arduino/cpp/%.o: $(ARDUINO_CORE_DIR)/%.cpp | $(BUILD_DIR)/arduino/cpp
	$(CXX) $(ARDUINO_CXXFLAGS) -c -o $@ $^

$(BUILD_DIR)/arduino/c $(BUILD_DIR)/arduino/cpp:
	mkdir -p $@
