#include <avr/io.h>

#define CONTROL   PORTC
#define OPERAND_1 PORTK
#define OPERAND_2 PORTF
#define RESULT    PINL

#define CLK14_PIN          21
#define OUTPUT_DISABLE_PIN 20

#define CONTROL_SET(bit, val) do { \
    if (val) {                     \
        CONTROL |= _BV(bit);       \
    } else {                       \
        CONTROL &= ~_BV(bit);      \
    }                              \
} while(0)

void io_init();
uint8_t do_operation(uint8_t a, uint8_t b);
void print_err(uint16_t a, uint16_t b, uint16_t actual, uint16_t expected, const char *op);
