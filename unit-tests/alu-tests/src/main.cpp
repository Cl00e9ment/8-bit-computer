#include <Arduino.h>

#include "io.hpp"
#include "test.h"

void setup(void) {
	io_init();

	Serial.println("starting tests");
	test_adder();
	test_bitwise_module();
	test_shifter();
	test_comparator();
	Serial.println("tests finished");
}

void loop(void) {}
