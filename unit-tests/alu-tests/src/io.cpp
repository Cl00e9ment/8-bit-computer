#include <Arduino.h>

#include "io.hpp"

void io_init() {
	DDRC = 0xff;
	DDRK = 0xff;
	DDRF = 0xff;

	pinMode(CLK14_PIN         , OUTPUT);
	pinMode(OUTPUT_DISABLE_PIN, OUTPUT);

	digitalWrite(OUTPUT_DISABLE_PIN, 1);

	Serial.begin(9600);
}

uint8_t do_operation(uint8_t a, uint8_t b) {
	OPERAND_1 = a;
	OPERAND_2 = b;
	asm volatile ("nop");
	asm volatile ("nop");
	asm volatile ("nop");
	asm volatile ("nop");
	asm volatile ("nop");
	asm volatile ("nop");
	asm volatile ("nop");
	asm volatile ("nop");
	asm volatile ("nop");
	asm volatile ("nop");
	asm volatile ("nop");
	asm volatile ("nop");
	asm volatile ("nop");
	asm volatile ("nop");
	asm volatile ("nop");
	uint8_t res = RESULT;
	return res;
}

void print_err(uint16_t a, uint16_t b, uint16_t actual, uint16_t expected, const char *op) {
    Serial.print("error: ");
    Serial.print(a, DEC);
    Serial.print(' ');
    Serial.print(op);
    Serial.print(' ');
    Serial.print(b, DEC);
    Serial.print(" = ");
    Serial.print(actual, DEC);
    Serial.print(" (expected: ");
    Serial.print(expected, DEC);
    Serial.println(")");
}
