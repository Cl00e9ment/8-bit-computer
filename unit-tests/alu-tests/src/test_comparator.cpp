#include <Arduino.h>

#include "io.hpp"
#include "test.h"

#define SIGN_BIT 4

void comparator_tests(bool k2, bool k3, bool is_signed) {
	CONTROL_SET(2, k2);
	CONTROL_SET(3, k3);
	CONTROL_SET(SIGN_BIT, is_signed);

	uint8_t a = 0;
	do {
		uint8_t b = 0;
		do {
			uint8_t expected;
			const char *op;

			switch ((k3 << 1) | k2) {
				case 0b00:
					expected = is_signed ? ((int8_t) a > (int8_t) b) : a > b;
					op = is_signed ? ">s" : ">u";
					break;

				case 0b01:
					expected = is_signed ? ((int8_t) a < (int8_t) b) : a < b;
					op = is_signed ? "<s" : "<u";
					break;

				case 0b10:
					expected = a == b;
					op = "==";
					break;

				case 0b11:
					expected = a != b;
					op = "!=";
					break;
			}

			uint8_t actual = do_operation(a, b);
			if (actual != expected) {
				print_err(a, b, actual, expected, op);
			}
			++b;
		} while (b != 0);
		++a;
	} while (a != 0);
}

void test_comparator() {
	CONTROL = 0b01100000;
	digitalWrite(OUTPUT_DISABLE_PIN, 0);
	comparator_tests(true , false, false);
	comparator_tests(true , true , false);
	comparator_tests(true , false, true);
	comparator_tests(true , true , true);
	comparator_tests(false, false, false);
	comparator_tests(false, true , false);
	digitalWrite(OUTPUT_DISABLE_PIN, 1);
}
