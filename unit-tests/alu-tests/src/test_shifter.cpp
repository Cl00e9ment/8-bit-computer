#include <Arduino.h>

#include "io.hpp"
#include "test.h"

#define SIGN_BIT      2
#define DIRECTION_BIT 3

void shift_tests(bool right, bool is_signed) {
	CONTROL_SET(DIRECTION_BIT, right);
	CONTROL_SET(SIGN_BIT, is_signed);

	uint8_t a = 0;
	do {
		for (uint8_t b = 0; b < 8; ++b) {
			uint8_t expected = right ? a >> b : a << b;

			if (is_signed && right && a >= 0b10000000) {
				expected |= ((1 << b) - 1) << (8 - b);
			}

			const char *op = right
				? (is_signed ? ">>s" : ">>u")
				: (is_signed ? "<<s" : "<<u");

			uint8_t actual = do_operation(a, b);
			if (actual != expected) {
				print_err(a, b, actual, expected, op);
			}
		}
		++a;
	} while (a != 0);
}

void test_shifter() {
	CONTROL = 0b00100000;
	digitalWrite(OUTPUT_DISABLE_PIN, 0);
	shift_tests(false, false);
	shift_tests(true, false);
	shift_tests(true, true);
	digitalWrite(OUTPUT_DISABLE_PIN, 1);
}
