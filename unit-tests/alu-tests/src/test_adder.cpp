#include <Arduino.h>

#include "io.hpp"
#include "test.h"

#define WITH_CARRY_BIT 2
#define SUBTRACT_BIT   3

uint16_t add16(uint16_t a, uint16_t b) {
	uint16_t result = do_operation(a & 0xff, b & 0xff);
	digitalWrite(CLK14_PIN, 1);
	digitalWrite(CLK14_PIN, 0);

	CONTROL_SET(WITH_CARRY_BIT, 1);

	result |= do_operation(a >> 8, b >> 8) << 8;
	digitalWrite(CLK14_PIN, 1);
	digitalWrite(CLK14_PIN, 0);

	CONTROL_SET(WITH_CARRY_BIT, 0);

	return result;
}

void add8_tests(bool subtract) {
	digitalWrite(CLK14_PIN, 0);
	CONTROL_SET(WITH_CARRY_BIT, 0);
	CONTROL_SET(SUBTRACT_BIT, subtract);

	uint8_t a = 0;
	uint8_t b = 0;
	do {
		do {
			uint8_t expected = subtract ? a - b : a + b;
			uint8_t actual = do_operation(a, b);
			if (actual != expected) {
				print_err(a, b, actual, expected, subtract ? "-" : "+");
			}
			++b;
		} while (b != 0);
		++a;
	} while (a != 0);
}

void add16_tests(boolean subtract) {
	digitalWrite(CLK14_PIN, 0);
	CONTROL_SET(WITH_CARRY_BIT, 0);
	CONTROL_SET(SUBTRACT_BIT, subtract);

	uint16_t a = 0;
	uint16_t b = 0b1010111110101000;
	do {
		uint16_t expected = subtract ? a - b : a + b;
		uint16_t actual = add16(a, b);
		if (actual != expected) {
			print_err(a, b, actual, expected, subtract ? "-" : "+");
		}
		++a;
	} while (a != 0);


	a = 0b1010111110101000;
	b = 0;
	do {
		uint16_t expected = subtract ? a - b : a + b;
		uint16_t actual = add16(a, b);
		if (actual != expected) {
			print_err(a, b, actual, expected, subtract ? "-" : "+");
		}
		++b;
	} while (b != 0);
}

void test_adder() {
	CONTROL = 0b00000000;
	digitalWrite(OUTPUT_DISABLE_PIN, 0);
	add8_tests(false);
	add16_tests(false);
	add8_tests(true);
	add16_tests(true);
	digitalWrite(OUTPUT_DISABLE_PIN, 1);
}
