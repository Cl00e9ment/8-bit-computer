#include <Arduino.h>

#include "io.hpp"
#include "test.h"

#define ENABLE_XOR_BIT 2
#define ENABLE_AND_BIT 3

void bitwise_tests(bool enable_and, bool enable_xor) {
	CONTROL_SET(ENABLE_AND_BIT, enable_and);
	CONTROL_SET(ENABLE_XOR_BIT, enable_xor);

	uint8_t a = 0;
	do {
		uint8_t b = 0;
		do {
			uint8_t expected;
			const char *op;

			if (enable_and && !enable_xor) {
				expected = a & b;
				op = "&";
			} else if (!enable_and && enable_xor) {
				expected = a ^ b;
				op = "^";
			} else if (enable_and && enable_xor) {
				expected = a | b;
				op = "|";
			} else {
				expected = 0;
				op = "?";
			}

			uint8_t actual = do_operation(a, b);
			if (actual != expected) {
				print_err(a, b, actual, expected, op);
			}
			++b;
		} while (b != 0);
		++a;
	} while (a != 0);
}

void test_bitwise_module() {
	CONTROL = 0b01000000;
	digitalWrite(OUTPUT_DISABLE_PIN, 0);
	bitwise_tests(true, false);
	bitwise_tests(false, true);
	bitwise_tests(true, true);
	digitalWrite(OUTPUT_DISABLE_PIN, 1);
}
