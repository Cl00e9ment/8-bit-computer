# CompCraft 1: an amateurish 8-bit computer

Side project where the goal is to build a 8-bit computer from "scratch" (74HC CMOS logic).

## Definition of done

- Being able to hook a PS/2 keyboard and VGA screen.
- Having a sheel to navigate and manipulate files.
- Having some utility on the 8-bit computer to edit sources, compile them and run the generated programs (without needing to flash an EEPROM or doing some fancy stuff).

## Bonus

- RS232 interface
- remote debugging
- sound card
- network controller
