#include <math.h>
#include <stdio.h>
#include <string.h>
#include <pigpio.h>
#include <unistd.h>

const unsigned ADDR_L[] = {25, 9, 10, 19, 16};
const unsigned ADDR_C[] = {13, 12, 6, 5, 7, 8, 11};
const unsigned IO[] = {3, 4, 17, 18, 27, 22, 23, 24};
const unsigned R_NW = 20;
const unsigned NOT_OE = 26;
const unsigned NOT_CE = 21;

void init_addr() {
	for (unsigned i = 0; i < 5; ++i) {
		gpioSetMode(ADDR_L[i], PI_OUTPUT);
	}
	for (unsigned i = 0; i < 7; ++i) {
		gpioSetMode(ADDR_C[i], PI_OUTPUT);
	}
}

void reset_addr() {
	// reset address bus to H-z
	for (unsigned i = 0; i < 5; ++i) {
		gpioSetMode(ADDR_L[i], PI_INPUT);
	}
	for (unsigned i = 0; i < 7; ++i) {
		gpioSetMode(ADDR_C[i], PI_INPUT);
	}
}

void init_io(unsigned mode) {
	for (unsigned i = 0; i < 8; ++i) {
		gpioSetMode(IO[i], mode);
	}
}

void set_addr(unsigned line, unsigned col) {
	for (unsigned i = 0; i < 5; ++i) {
		gpioWrite(ADDR_L[i], line & 1);
		line >>= 1;
	}
	for (unsigned i = 0; i < 7; ++i) {
		gpioWrite(ADDR_C[i], col & 1);
		col >>= 1;
	}
}

void write_data(char data) {
	for (int i = 7; i >= 0; --i) {
		gpioWrite(IO[i], data & 1);
		data >>= 1;
	}
}

char read_data() {
	char data = 0;
	for (int i = 7; i >= 0; --i) {
		data = (data << 1) | gpioRead(IO[i]);
	}
	return data;
}

void print_charset() {
	for (unsigned line = 0; line < 25; ++line) {
		for (unsigned col = 0; col < 80; ++col) {
			set_addr(line, col);
			write_data((line >= 16 || col >= 16) ? '*' : (line * 16 + col % 16));
			gpioWrite(R_NW, 0);
			gpioWrite(R_NW, 1);
		}
	}
}

void clear(char c) {
	for (unsigned line = 0; line < 25; ++line) {
		for (unsigned col = 0; col < 80; ++col) {
			set_addr(line, col);
			write_data(c);
			gpioWrite(R_NW, 0);
			gpioWrite(R_NW, 1);
		}
	}
}

void print_str(char *str) {
	unsigned col = 0;
	unsigned line = 0;
	while (*str) {
		if (*str == '\n') {
			col = 0;
			++line;
			++str;
			continue;
		}
		set_addr(line, col);
		write_data(*str);
		gpioWrite(R_NW, 0);
		gpioWrite(R_NW, 1);
		if (col < 80) {
			++col;
		} else {
			col = 0;
			++line;
		}
		++str;
	}
}

void draw_line(char buffer[100][160], float x0, float y0, float x1, float y1) {
	float vx = x1 - x0;
	float vy = y1 - y0;
	float len = sqrt(vx * vx + vy * vy);
	vx /= len;
	vy /= len;
	for (float n = 0; n <= len; ++n) {
		buffer[(int) (y0 + n * vy)][(int) (x0 + n * vx)] = 1;
	}
}

void print_buffer(char buffer[100][160]) {
	for (int line = 0; line < 25; ++line) {
		for (int col = 0; col < 80; ++col) {
			char c = 0;
			for (int i = 0; i < 8; ++i) {
				if (buffer[line * 4 + i / 2][col * 2 + i % 2]) {
					c |= 1 << i;
				}
			}
			set_addr(line, col);
			write_data(c);
			gpioWrite(R_NW, 0);
			gpioWrite(R_NW, 1);
		}
	}
}

void draw_diags() {
	char buffer[100][160];
	memset(buffer, 0, 16000);
	draw_line(buffer, 0, 0, 160, 100);
	draw_line(buffer, 160, 0, 0, 100);
	print_buffer(buffer);
}

void vec_matrix_multiply(float dst[3], float matrix[3][3], float vec[3]) {
	dst[0] = matrix[0][0] * vec[0] + matrix[0][1] * vec[1] + matrix[0][2] * vec[2];
	dst[1] = matrix[1][0] * vec[0] + matrix[1][1] * vec[1] + matrix[1][2] * vec[2];
	dst[2] = matrix[2][0] * vec[0] + matrix[2][1] * vec[1] + matrix[2][2] * vec[2];
}

void matrix_multiply(float m0[3][3], float m1[3][3]) {
	float res[3][3];

	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < 3; ++j) {
			float sum = 0;
			for (int k = 0; k < 3; ++k) {
				sum += m0[i][k] * m1[k][j];
			}
			res[i][j] = sum;
		}
	}

	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < 3; ++j) {
			m0[i][j] = res[i][j];
		}
	}
}

void draw_cube(float size, float angleX, float angleY, float angleZ) {
	size /= 2;

	char buffer[100][160];
	memset(buffer, 0, 16000);

	float vertices[12][2][3] = {
		{{-1, -1, -1}, { 1, -1, -1}},
		{{-1,  1, -1}, { 1,  1, -1}},
		{{-1, -1, -1}, {-1,  1, -1}},
		{{ 1, -1, -1}, { 1,  1, -1}},
		{{-1, -1,  1}, { 1, -1,  1}},
		{{-1,  1,  1}, { 1,  1,  1}},
		{{-1, -1,  1}, {-1,  1,  1}},
		{{ 1, -1,  1}, { 1,  1,  1}},
		{{-1, -1, -1}, {-1, -1,  1}},
		{{-1,  1, -1}, {-1,  1,  1}},
		{{ 1, -1, -1}, { 1, -1,  1}},
		{{ 1,  1, -1}, { 1,  1,  1}}
	};

	float rot_matrix[3][3] = {
		{1, 0, 0},
		{0, cos(angleX), -sin(angleX)},
		{0, sin(angleX), cos(angleX)}
	};

	float rot_matrix_y[3][3] = {
		{cos(angleY), 0, sin(angleY)},
		{0, 1, 0},
		{-sin(angleY), 0, cos(angleY)}
	};

	float rot_matrix_z[3][3] = {
		{cos(angleZ), -sin(angleZ), 0},
		{sin(angleZ), cos(angleZ), 0},
		{0, 0, 1}
	};

	matrix_multiply(rot_matrix, rot_matrix_y);
	matrix_multiply(rot_matrix, rot_matrix_z);
	
	for (int i = 0; i < 12; ++i) {
		float vertex0[3], vertex1[3];
		vec_matrix_multiply(vertex0, rot_matrix, vertices[i][0]);
		vec_matrix_multiply(vertex1, rot_matrix, vertices[i][1]);

		draw_line(
				buffer,
				vertex0[0] * size + 80,
				vertex0[1] * size + 50,
				vertex1[0] * size + 80,
				vertex1[1] * size + 50
		);
	}

	print_buffer(buffer);
}

int main() {
	if (gpioInitialise() < 0) {
		return 1;
	}

	// disable everything
	gpioSetMode(NOT_CE, PI_OUTPUT);
	gpioSetMode(R_NW, PI_OUTPUT);
	gpioSetMode(NOT_OE, PI_OUTPUT);
	gpioWrite(NOT_CE, 1);
	gpioWrite(R_NW, 1);
	gpioWrite(NOT_OE, 1);

	// wait to be sure
	usleep(150);

	// now we can safelly output things
	gpioWrite(NOT_CE, 0);
	init_addr();
	init_io(PI_OUTPUT);
	usleep(150);

	// write some data
	//clear(' ');
	//print_str(" @\n \x0c");
	//print_str("Lorem Ipsum");
//	print_charset();
//	sleep(1000);

	float step = 2 * M_PI / 60;
	float angleX = 0;
	float angleY = 0;
	float angleZ = 0;
	while (1) {
		draw_cube(50, angleX, angleY, angleZ);
		angleX += step;
		angleY += step / 2;
		angleZ += step / 3;
		usleep(1000000 / 60);
	}

	sleep(100);

	// reset everything to H-z
	reset_addr();
	init_io(PI_INPUT);
	gpioSetMode(NOT_CE, PI_INPUT);
	gpioSetMode(R_NW, PI_INPUT);
	gpioSetMode(NOT_OE, PI_INPUT);

	gpioTerminate();
	return 0;
}
