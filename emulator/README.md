# CompCraft 1 Emulator

## How to use?

1. [install Rust](https://www.rust-lang.org/tools/install)
2. add `$USER/.cargo/bin` to your `$PATH`
3. compile & install emulator: `cargo install --path .`
4. get a binary to run (see [assembler](../assembler))
5. run the binary in the emulator: `compcraft-emulator a.out`

## How to develop?

1. [install Rust](https://www.rust-lang.org/tools/install)
2. modify sources
4. get a binary to run (see [assembler](../assembler))
5. test the emulator by tunning the binary: `cargo run a.out`
