mod bit_utils;
mod cpu;
mod devices;

use std::{thread, env, process};
use std::time::{SystemTime, Duration};

use cpu::Cpu;
use devices::Devices;

fn main() {
	let args = env::args().collect::<Vec<_>>();
	if args.len() != 2 {
		println!("error: one and only one argument expected (path to binary to execute)");
		process::exit(1);
	}

	let mut cpu = Cpu::default();
	let mut devices = Devices::default();

	devices.init_mem_from_file(&args[1]);
	cpu.reset();

	let mut cycle_count = 0;
	let start = SystemTime::now();

	while !devices.stop_requested() {
		cpu.cycle(&mut devices);

		// run the emulator at 2MHz
		cycle_count += 1;
		let to_sleep = 500 - (start.elapsed().unwrap().as_nanos() / cycle_count) as i128;
		if to_sleep > 0 {
			thread::sleep(Duration::from_nanos(to_sleep as u64));
		}
	}

	println!("avg cycle time: {}ns", start.elapsed().unwrap().as_nanos() / cycle_count);
}
