pub trait BitManipulation {
	fn bit(idx: u8) -> Self;
	fn bit_set(&self, idx: u8) -> bool;
	fn bits_set(&self, bits: Self) -> bool;
	fn set_bit(&mut self, idx: u8);
	fn clear_bit(&mut self, idx: u8);
}

macro_rules! bit_manipulation_impl {
	($t:ty) => {
		impl BitManipulation for $t {
			#[inline]
			fn bit(idx: u8) -> Self {
				1 << idx
			}

			#[inline]
			fn bit_set(&self, idx: u8) -> bool {
				self & Self::bit(idx) != 0
			}

			#[inline]
			fn bits_set(&self, bits: Self) -> bool {
				self & bits == bits
			}

			#[inline]
			fn set_bit(&mut self, idx: u8) {
				*self |= Self::bit(idx);
			}

			#[inline]
			fn clear_bit(&mut self, idx: u8) {
				*self &= !Self::bit(idx);
			}
		}
	};
}

bit_manipulation_impl!(u8);
bit_manipulation_impl!(u16);

pub trait ByteSplit {
	fn msb(&self) -> u8;
	fn lsb(&self) -> u8;
	fn set_msb(&mut self, value: u8);
	fn set_lsb(&mut self, value: u8);
}

impl ByteSplit for u16 {
	#[inline]
	fn msb(&self) -> u8 {
		(self >> 8) as u8
	}

	#[inline]
	fn lsb(&self) -> u8 {
		*self as u8
	}

	#[inline]
	fn set_msb(&mut self, value: u8) {
		*self = *self & 0x00ff | (value as u16) << 8;
	}

	#[inline]
	fn set_lsb(&mut self, value: u8) {
		*self = *self & 0xff00 | value as u16;
	}
}
