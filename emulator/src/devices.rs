use std::{fs, process};
use std::io::{stdout, Write};

pub struct Devices {
	memory: [u8; 1<<16],
	stop_requested: bool,
}

impl Default for Devices {
	fn default() -> Self {
		Self {
			memory: [0; 1 << 16],
			stop_requested: false,
		}
	}
}

impl Devices {
	pub fn init_mem_from_file(&mut self, path: &str) {
		let content = fs::read(path).unwrap_or_else(|e| {
			println!("error: failed to read file {}: {}", path, e.to_string());
			process::exit(1);
		});

		if content.len() > self.memory.len() {
			println!("error: provided binary size ({} bytes) is larger than memory ({} bytes)", content.len(), self.memory.len());
			process::exit(1);
		}

		if content.len() > self.memory.len() / 2 {
			println!("warning: provided binary size ({} bytes) is larger than half of the memory ({} bytes)", content.len(), self.memory.len() / 2);
		}

		self.memory[..content.len()].copy_from_slice(&content);
	}

	pub fn read(&self, addr: u16) -> u8 {
		self.memory[addr as usize]
	}

	pub fn write(&mut self, addr: u16, value: u8) {
		match addr {
			// RAM
			0x0000..=0x7fff | 0x8100..=0xffff => self.memory[addr as usize] = value,
			// expantion card: RS232 write
			0x8000 => {
				print!("{}", value as char);
				_ = stdout().flush();
			},
			// expantion card: emulator only feature: stop the emulator
			0x80ff => self.stop_requested = value != 0,
			_ => {},
		}
	}

	pub fn stop_requested(&self) -> bool {
		self.stop_requested
	}
}
