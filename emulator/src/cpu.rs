use std::process;

use crate::bit_utils::{BitManipulation, ByteSplit};
use crate::devices::Devices;

#[derive(Default)]
pub struct Cpu {
	instr  : u16,
	lip_buf: u8,
	ip     : u16,
	sp     : u16,
	ac     : u8,
	ad     : u16,
	alm    : u32,
	ira    : u16,
	cf1    : bool,
	cf2    : bool,
	ien    : bool,
	interrupt_requests: u8,
}

impl Cpu {
	pub fn reset(&mut self) {
		self.ip  = 0;
		self.sp  = 0;
		self.cf1 = false;
		self.cf2 = false;
		self.alm = 0;
		self.interrupt_requests = 0;
	}

	pub fn request_interrupt(&mut self, interrupt_idx: u8) {
		assert!((1..=3).contains(&interrupt_idx));
		self.interrupt_requests.set_bit(interrupt_idx);
	}

	pub fn cycle(&mut self, devices: &mut Devices) {
		if self.ien && self.interrupt_requests != 0 {
			let interrupt_id = self.interrupt_requests.trailing_zeros() as u8; // get the index of the first bit set (from the right) of the bitfield
			self.interrupt_requests.clear_bit(interrupt_id);
			self.ira = self.ip;
			self.ip = 0b1000 | u16::bit(interrupt_id);
			self.ien = false;
		}

		if self.alm > 0 {
			self.alm -= 1;
			if self.alm == 0 {
				self.interrupt_requests.set_bit(0);
			}
		}

		self.instr = u16::from_be_bytes([
			devices.read(self.ip),
			devices.read(self.ip + 1),
		]);

		self.ip = self.ip.wrapping_add(2);

		// jmp/jnz
		if self.instr.bit_set(15) {
			if self.instr.bit_set(0) || self.ac.bit_set(0) {
				let mut offset = self.instr & !(u16::bit(15) | u16::bit(0));
				if offset.bit_set(14) {
					offset |= u16::bit(15);
				}
				self.ip = self.ip.wrapping_add(offset);
			}
			return;
		}

		// add/addc/sub/subb
		if self.instr & 0b11110000_00000000 == 0 {
			let mut operand = self.get_operand_value(devices);

			// sub/subb
			if self.instr.bit_set(11) {
				operand = !operand;
			}

			let mut increment = 0;

			// sub
			if self.instr & 0b00001100_00000000 == 0b00001000_00000000 {
				increment += 1;
			}

			// addc/subb and cf1 set
			if self.instr.bit_set(10) && self.cf1 {
				increment += 1;
			}

			(operand, self.cf1) = operand.overflowing_add(increment);
			(self.ac, self.cf2) = self.ac.overflowing_add(operand);
			return;
		}

		let opcode = self.instr & 0b11111100_00000000;

		// shl
		if opcode == 0b00100000_00000000 {
			self.ac <<= self.get_operand_value(devices) % 8;
			return;
		}

		// shr
		if opcode == 0b00101000_00000000 {
			self.ac >>= self.get_operand_value(devices) % 8;
			return;
		}

		// sshr
		if opcode == 0b00101100_00000000 {
			self.ac = ((self.ac as i8) >> (self.get_operand_value(devices) % 8)) as u8;
			return;
		}

		// or
		if opcode == 0b01001100_00000000 {
			self.ac |= self.get_operand_value(devices);
			return;
		}

		// and
		if opcode == 0b01001000_00000000 {
			self.ac &= self.get_operand_value(devices);
			return;
		}

		// xor
		if opcode == 0b01000100_00000000 {
			self.ac ^= self.get_operand_value(devices);
			return;
		}

		// gtu
		if opcode == 0b01100000_00000000 {
			self.ac = if self.ac > self.get_operand_value(devices) { 1 } else { 0 };
			return;
		}

		// ltu
		if opcode == 0b01100100_00000000 {
			self.ac = if self.ac < self.get_operand_value(devices) { 1 } else { 0 };
			return;
		}

		// gts
		if opcode == 0b01110000_00000000 {
			self.ac = if self.ac as i8 > self.get_operand_value(devices) as i8 { 1 } else { 0 };
			return;
		}

		// lts
		if opcode == 0b01110100_00000000 {
			self.ac = if (self.ac as i8) < self.get_operand_value(devices) as i8 { 1 } else { 0 };
			return;
		}

		// equ
		if self.instr & 0b11101100_00000000 == 0b01101000_00000000 {
			self.ac = if self.ac == self.get_operand_value(devices) { 1 } else { 0 };
			return;
		}

		// neq
		if self.instr & 0b11101100_00000000 == 0b01101100_00000000 {
			self.ac = if self.ac != self.get_operand_value(devices) { 1 } else { 0 };
			return;
		}

		// load
		if opcode == 0b00010000_00000000 {
			self.ac = self.get_operand_value(devices);
			return;
		}

		// store
		if opcode == 0b00010100_00000000 {
			self.set_operand_value(devices, false, self.ac);
			return;
		}

		// push
		if opcode == 0b00011000_00000000 {
			let operand_type = self.instr & 0b00000011_00000000;
			if operand_type == 0b00000001_00000000 || operand_type == 0b00000010_00000000 {
				self.illegal_instruction();
			}
			let operand_value = self.get_operand_value(devices);
			self.sp = self.sp.wrapping_sub(1);
			devices.write(self.sp, operand_value);
			return;
		}

		// pop
		if opcode == 0b00011100_00000000 {
			self.set_operand_value(devices, true, devices.read(self.sp));
			self.sp = self.sp.wrapping_add(1);
			return;
		}

		// alm
		if opcode == 0b01010000_00000000 {
			self.alm = (self.get_operand_value(devices) as u32) << 12;
			return;
		}

		// iret
		if opcode == 0b01010100_00000000 {
			self.ip = self.ira;
			self.ien = true;
			return;
		}

		// din
		if opcode == 0b01011000_00000000 {
			self.ien = false;
			return;
		}

		// ein
		if opcode == 0b01011100_00000000 {
			self.ien = true;
			return;
		}

		self.illegal_instruction();
	}

	fn get_operand_value(&self, devices: &mut Devices) -> u8 {
		match self.instr & 0b000011_00000000 {
			0b00000000_00000000 => self.instr.lsb(),
			0b00000001_00000000 => devices.read(self.sp.wrapping_add(self.instr.lsb() as u16)),
			0b00000010_00000000 => devices.read(self.ad.wrapping_add(self.instr.lsb() as u16)),
			0b00000011_00000000 =>
				match self.instr & 0b00000000_11100000 {
					0b00000000 => self.ip.wrapping_add((self.instr & 0b1111) << 1).msb(),
					0b00100000 => self.ip.wrapping_add((self.instr & 0b1111) << 1).lsb(),
					0b01000000 => self.ad.msb(),
					0b01100000 => self.ad.lsb(),
					0b10000000 => self.ac,
					_ => self.illegal_instruction(),
				},
			_ => self.illegal_instruction(),
		}
	}

	fn set_operand_value(&mut self, devices: &mut Devices, pop: bool, value: u8) {
		match self.instr & 0b000011_00000000 {
			0b00000000_00000000 if pop => {}
			0b00000001_00000000 if !pop => devices.write(self.sp.wrapping_add(self.instr.lsb() as u16), value),
			0b00000010_00000000 if !pop => devices.write(self.ad.wrapping_add(self.instr.lsb() as u16), value),
			0b00000011_00000000 =>
				match self.instr & 0b00000000_11100000 {
					0b00000000 => self.ip = ((value as u16) << 8) | (self.lip_buf as u16),
					0b00100000 => self.lip_buf = value,
					0b01000000 => self.ad.set_msb(value),
					0b01100000 => self.ad.set_lsb(value),
					0b10000000 => self.ac = value,
					_ => self.illegal_instruction(),
				},
			_ => self.illegal_instruction(),
		};
	}

	fn illegal_instruction(&self) -> ! {
		println!("error: illegal instruction: {:#06x}", self.instr);
		println!("IP: {:#06x}", self.ip);
		process::exit(1);
	}
}
